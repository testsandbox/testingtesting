// XAG49_ToolboxDlg.h : header file
//

#if !defined(AFX_XAG49_TOOLBOXDLG_H__04BF8BF8_DB22_4D69_924E_E8466438B274__INCLUDED_)
#define AFX_XAG49_TOOLBOXDLG_H__04BF8BF8_DB22_4D69_924E_E8466438B274__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CXAG49_ToolboxDlg dialog

class CXAG49_ToolboxDlg : public CDialog
{
// Construction
public:
	CXAG49_ToolboxDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CXAG49_ToolboxDlg)
	enum { IDD = IDD_XAG49_TOOLBOX_DIALOG };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXAG49_ToolboxDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CXAG49_ToolboxDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XAG49_TOOLBOXDLG_H__04BF8BF8_DB22_4D69_924E_E8466438B274__INCLUDED_)
