//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by XAG49_Toolbox.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_XAG49_TOOLBOX_DIALOG        102
#define IDD_MEMACESS_DIALOG             103
#define IDD_CONNECT_DIALOG              107
#define IDR_MAINFRAME                   128
#define IDD_SCRIPT_DIALOG               129
#define IDD_MEMDUMP_DIALOG              130
#define IDC_EDITSTATUS                  1000
#define IDC_EDITLOOP                    1000
#define IDC_EDITSTATUSSCRIPT            1001
#define IDC_EDITLOOPDELAY               1001
#define IDC_BUTTONEDITTEXTFILE          1002
#define IDC_EDITSTATUSDUMP              1002
#define IDC_BUTTONRUNSCRIPT             1003
#define IDC_BUTTONREAD                  1003
#define IDC_BUTTONLOADADDRESS           1004
#define IDC_BUTTONCLEARSCRIPT           1004
#define IDC_EDITENDADDR                 1004
#define IDC_EDITCOMPORT                 1005
#define IDC_BUTTONEDITSCRIPTFILE        1005
#define IDC_BUTTONCLEARDISPLAY          1005
#define IDC_COMBOACTIONS                1006
#define IDC_EDITSTARTADDR               1006
#define IDC_EDITPARAM1                  1007
#define IDC_COMBOMEMTYPE                1007
#define IDC_EDITPARAM2                  1008
#define IDC_EDITMEMDUMP                 1008
#define IDC_EDITPARAM3                  1009
#define IDC_EDITCMDINDEX                1010
#define IDC_SPINSCRIPTINDEX             1011
#define IDC_EDIT_BLSCRIPT               1012
#define IDC_BUTTONRUNBLSCRIPT           1013
#define IDC_BUTTONADDCOMMAND            1014
#define IDC_BUTTONREMOVECOMMAND         1015
#define IDC_EDITPARAM1HINT              1016
#define IDC_EDITPARAM2HINT              1017
#define IDC_EDITPARAM3HINT              1018
#define IDC_BUTTONINSERTCOMMAND         1019
#define IDC_BUTTONUPDATECOMMAND         1020
#define IDC_EDITPARAM4                  1021
#define IDC_BUTTONSENDSCRIPT            1022
#define IDC_BUTTONSTOPDATAMONITOR       1023
#define IDCONNECT                       1024
#define IDC_BUTTONSAVESCRIPT            1024
#define IDC_BUTTONLOADSCRIPT            1025
#define IDC_EDITPARAM5                  1026
#define IDC_EDITPARAM6                  1027
#define IDC_EDITPARAM4HINT              1028
#define IDC_EDITPARAM5HINT              1029
#define IDC_EDITSTATUSACCESS            1030
#define IDC_EDITPARAM6HINT              1030
#define IDC_EDITPARAM7                  1031
#define IDC_CHECKSAFETY                 1032
#define IDC_EDITPARAM7HINT              1032
#define IDC_CHECKADVBOOTLOADER          1033
#define IDC_EDITPARAM8                  1033
#define IDC_EDITPARAM8HINT              1034
#define IDC_EDITSCRIPTFILENAME          1039
#define IDC_CHECKLOGENABLE              1043
#define IDC_SCROLLBAR1                  1043
#define IDC_BUTTONGETINFO               1044
#define IDC_STATIC_MEMTYPE              1044
#define IDC_STATIC_ADDR                 1045
#define IDC_STATIC_DATAVAL              1046
#define IDC_BUTTONCLR                   1047
#define IDC_STATIC_LOOPEN               1047
#define IDC_BUTTONCONTREAD              1048
#define IDC_STATIC_P1                   1049
#define IDC_BUTTONCONTSTOP              1050
#define IDC_STATIC_P2                   1051
#define IDC_STATIC_P3                   1052
#define IDC_CHECKSTOPERROR              1053
#define IDC_PIUINFO                     1054
#define IDC_STATIC_P4                   1054
#define IDC_STATIC_P5                   1055
#define IDC_STATIC_P6                   1056

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1009
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
