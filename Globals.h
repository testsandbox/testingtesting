#if !defined(IUADPDIAG_GLOBALS__INCLUDED_)
#define IUADPDIAG_GLOBALS__INCLUDED_

#include "Commands.h"
#include "SerialComm.h"
#include <fstream>

extern CCommands commandEngine;
extern CSerialComm serialObj;
extern CString szAddrFileName;

extern bool bEnableSafetyCriticalWrites;
extern bool bDataLoggingEnabled;
extern bool bErrorDetected;
extern bool bAdvancedBootloaderAvailable;
extern bool bComEstablished;
extern std::ofstream logfile;
extern int NumberOfAvailableInputFiles;

#endif