// XAG49_Toolbox.h : main header file for the XAG49_TOOLBOX application
//

#if !defined(AFX_XAG49_TOOLBOX_H__13DDEC8C_29FE_4AEC_BCC0_252C694315D1__INCLUDED_)
#define AFX_XAG49_TOOLBOX_H__13DDEC8C_29FE_4AEC_BCC0_252C694315D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CXAG49_ToolboxApp:
// See XAG49_Toolbox.cpp for the implementation of this class
//

class CXAG49_ToolboxApp : public CWinApp
{
public:
	CXAG49_ToolboxApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CXAG49_ToolboxApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CXAG49_ToolboxApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_XAG49_TOOLBOX_H__13DDEC8C_29FE_4AEC_BCC0_252C694315D1__INCLUDED_)
