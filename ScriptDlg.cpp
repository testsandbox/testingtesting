// ScriptDlg.cpp : implementation file
//

#include "stdafx.h"
#include "XAG49_Toolbox.h"
#include "ScriptDlg.h"
#include "SerialComm.h"
#include "Globals.h"
#include <fstream>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CScriptDlg property page

IMPLEMENT_DYNCREATE(CScriptDlg, CPropertyPage)

CScriptDlg::CScriptDlg() : CPropertyPage(CScriptDlg::IDD)
{
	//{{AFX_DATA_INIT(CScriptDlg)
	m_Status = _T("");
	m_StackDisplay = _T("");
	//}}AFX_DATA_INIT
}

CScriptDlg::~CScriptDlg()
{
}

void CScriptDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CScriptDlg)
	DDX_Control(pDX, IDC_EDITPARAM7HINT, m_EditParam7Hint);
	DDX_Control(pDX, IDC_EDITPARAM8HINT, m_EditParam8Hint);
	DDX_Control(pDX, IDC_EDITPARAM8, m_EditParam8);
	DDX_Control(pDX, IDC_EDITPARAM7, m_EditParam7);
	DDX_Control(pDX, IDC_EDITPARAM6HINT, m_EditParam6Hint);
	DDX_Control(pDX, IDC_EDITPARAM6, m_EditParam6);
	DDX_Control(pDX, IDC_EDITPARAM5HINT, m_EditParam5Hint);
	DDX_Control(pDX, IDC_EDITPARAM5, m_EditParam5);
	DDX_Control(pDX, IDC_EDITPARAM4HINT, m_EditParam4Hint);
	DDX_Control(pDX, IDC_EDITPARAM4, m_EditParam4);
	DDX_Control(pDX, IDC_SPINSCRIPTINDEX, m_SpinStackIndex);
	DDX_Control(pDX, IDC_EDITPARAM3HINT, m_EditParam3Hint);
	DDX_Control(pDX, IDC_EDITPARAM2HINT, m_EditParam2Hint);
	DDX_Control(pDX, IDC_EDITPARAM1HINT, m_EditParam1Hint);
	DDX_Control(pDX, IDC_EDITPARAM3, m_EditParam3);
	DDX_Control(pDX, IDC_EDITPARAM2, m_EditParam2);
	DDX_Control(pDX, IDC_EDITPARAM1, m_EditParam1);
	DDX_Control(pDX, IDC_COMBOACTIONS, m_ComboCommand);
	DDX_Text(pDX, IDC_EDITSTATUSSCRIPT, m_Status);
	DDX_Text(pDX, IDC_EDIT_BLSCRIPT, m_StackDisplay);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CScriptDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CScriptDlg)
	ON_BN_CLICKED(IDC_BUTTONRUNSCRIPT, OnButtonrunscript)
	ON_BN_CLICKED(IDC_BUTTONEDITSCRIPTFILE, OnButtoneditscriptfile)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTONADDCOMMAND, OnButtonaddcommand)
	ON_BN_CLICKED(IDC_BUTTONREMOVECOMMAND, OnButtonremovecommand)
	ON_BN_CLICKED(IDC_BUTTONRUNBLSCRIPT, OnButtonrunblscript)
	ON_BN_CLICKED(IDC_BUTTONCLEARSCRIPT, OnButtonclearscript)
	ON_CBN_SELCHANGE(IDC_COMBOACTIONS, OnSelchangeComboactions)
	ON_NOTIFY(UDN_DELTAPOS, IDC_SPINSCRIPTINDEX, OnDeltaposSpinscriptindex)
	ON_BN_CLICKED(IDC_BUTTONINSERTCOMMAND, OnButtoninsertcommand)
	ON_BN_CLICKED(IDC_BUTTONUPDATECOMMAND, OnButtonupdatecommand)
	ON_BN_CLICKED(IDC_BUTTONSENDSCRIPT, OnButtonsendscript)
	ON_BN_CLICKED(IDC_BUTTONSTOPDATAMONITOR, OnButtonstopdatamonitor)
	ON_BN_CLICKED(IDC_BUTTONSAVESCRIPT, OnButtonsavescript)
	ON_BN_CLICKED(IDC_BUTTONLOADSCRIPT, OnButtonloadscript)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CScriptDlg message handlers

// Open, parse, and run a PeekPoke script (Read, Write, and Pause)
void CScriptDlg::OnButtonrunscript() 
{
	CString filename;
	// Launch the file open dialog to get file name

	static char BASED_CODE szFilter[] = "Script Files (*.txt)|*.txt|All Files (*.*)|*.*|";
	CFileDialog dlgFile(TRUE,NULL,NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter);
	if(dlgFile.DoModal() == IDOK)
	{
		filename = dlgFile.GetPathName();
		
		bool bOkToContinue = TRUE;
		char buf[120];
		CString line;
		CString token;
		int nIndexComma;
		nScriptIndex = 0;

		std::ifstream addrfile;
		addrfile.open(filename);

		if( addrfile.is_open() ) {
			while( !addrfile.eof() && nScriptIndex < NUM_OF_SCRIPT_ENTRIES) {
				bOkToContinue = TRUE;

				// Read in line
				addrfile >> buf;
				line = buf;

				// Extract Action
				nIndexComma = line.Find(",",0);
				
				if(line.Find("R",0) != -1) {
					StepData[nScriptIndex].eActionType = READ;
				} else if(line.Find("W",0) != -1) {
					StepData[nScriptIndex].eActionType = WRITE;
				} else if(line.Find("P",0) != -1) {
					StepData[nScriptIndex].eActionType = PAUSE;
				} else {
					bOkToContinue = FALSE;
				}

				// Handle READ action
				if(bOkToContinue && StepData[nScriptIndex].eActionType == READ) {
					if(nIndexComma != -1) {
						line.Delete(0,nIndexComma+1);
						StepData[nScriptIndex].uAddress = strtol(line, NULL, 16);
						nScriptIndex++;
					}
				}

				// Handle WRITE action
				else if(bOkToContinue && StepData[nScriptIndex].eActionType == WRITE) {
					if(nIndexComma != -1) {
						line.Delete(0,nIndexComma+1);
						StepData[nScriptIndex].uAddress = strtol(line, NULL, 16);

						nIndexComma = line.Find(",",0);
						if(nIndexComma != -1) {
							line.Delete(0,nIndexComma+1);
							StepData[nScriptIndex].uData = strtol(line, NULL, 16);
							nScriptIndex++;
						}
					}
				}

				// Handle PAUSE action
				else if(bOkToContinue && StepData[nScriptIndex].eActionType == PAUSE) {
					nScriptIndex++;
				}
			}
		
			addrfile.close();
		}
	}

	// If advanced bootloader is available, set the memory area first
	bErrorDetected = false;
	if(bAdvancedBootloaderAvailable) {
		MemoryArea ma = DisableAll;

		if (commandEngine.SelectMemory(ma)) {
        	commandEngine.GetErrorMsgString(m_Status);
        	bErrorDetected = true;
		}
	}

	if(!bErrorDetected) {
		// Now start timer to run the list
		nCurrentScriptIndex = 0;
		SetTimer(PEEKPOKE_SCRIPT_EXEC_TIMER, 250, NULL);	
	}
}

// Launches notepad to edit a PeekPoke script
void CScriptDlg::OnButtoneditscriptfile() 
{
	CString filename;
	// Launch the file open dialog to get file name

	static char BASED_CODE szFilter[] = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*|";
	CFileDialog dlgFile(TRUE,NULL,NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter);
	if(dlgFile.DoModal() == IDOK)
	{
		filename = dlgFile.GetPathName();
		ShellExecute(NULL, _T("open"), _T("notepad.exe"),filename, NULL, SW_SHOWNORMAL);
	}	
}

void CScriptDlg::OnTimer(UINT nIDEvent) 
{
	WORD AddressContents;
	CString address;
	CString data;
	CString str;
	CButton * pBtn;
	bool bGotData = false;

    switch (nIDEvent) {

	// Test script execution timer
    case PEEKPOKE_SCRIPT_EXEC_TIMER:

	    // Stop Timer Until Finished with action
	    KillTimer(PEEKPOKE_SCRIPT_EXEC_TIMER);

		// If there are more steps to execute
	    if(nCurrentScriptIndex <= nScriptIndex) {

			// Handle Pause Action
		    if(StepData[nCurrentScriptIndex].eActionType == PAUSE) {
			    MessageBox("Click OK to continue running the script");
			    nCurrentScriptIndex++;
			    SetTimer(PEEKPOKE_SCRIPT_EXEC_TIMER,250,NULL);

			// Handle Read Memory Action
		    } else if(StepData[nCurrentScriptIndex].eActionType == READ) {
                if(commandEngine.Peek( StepData[nCurrentScriptIndex].uAddress, &AddressContents )) {

			        data.Format("0x%X", AddressContents);
			        address.Format("0x%X", StepData[nCurrentScriptIndex].uAddress);

			        m_Status = "";
			        m_Status = "Address " + address + "=" + data;
                } else {
                    commandEngine.GetErrorMsgString(m_Status);
                }

			    if(bDataLoggingEnabled) {
			      logfile << (LPCTSTR)m_Status << std::endl;
			    }

			    // update the dialog
			    UpdateData(FALSE);

			    nCurrentScriptIndex++;
			    SetTimer(PEEKPOKE_SCRIPT_EXEC_TIMER,250,NULL);

			// Handle Write to Memory Action
		    } else if(StepData[nCurrentScriptIndex].eActionType == WRITE) {
			    if(commandEngine.Poke( StepData[nCurrentScriptIndex].uAddress, 
				                    StepData[nCurrentScriptIndex].uData, 
                                    bEnableSafetyCriticalWrites )) {

			        data.Format("0x%X", StepData[nCurrentScriptIndex].uData);
			        address.Format("0x%X", StepData[nCurrentScriptIndex].uAddress);

			        m_Status = "";
			        m_Status = data + " written to address " + address;
                } else {
                    commandEngine.GetErrorMsgString(m_Status);
                }

			    if(bDataLoggingEnabled) {
			      logfile << (LPCTSTR)m_Status << std::endl;
			    }

			    // update the dialog
			    UpdateData(FALSE);

			    nCurrentScriptIndex++;
			    SetTimer(PEEKPOKE_SCRIPT_EXEC_TIMER,250,NULL);
		    }
	    }
        break;

	case BL_SCRIPT_EXECUTION_TIMER:
		// Stop Timer Until Finished Processing
	    KillTimer(BL_SCRIPT_EXECUTION_TIMER);
	
		// Ping state machine for status and data
		if(commandEngine.ExecuteBLScript(bGotData, &AddressContents)) {
			// if data was received, display and test for script completion
			if(bGotData) {
				// Test if script complete
				if( (AddressContents & 0xFF00) == 0x0800) {
					m_Status ="Bootloader script completed";

					pBtn = (CButton *)GetDlgItem(IDC_BUTTONRUNBLSCRIPT);
					pBtn->EnableWindow(TRUE);

					// Update Displays
					UpdateData(FALSE);
				} else {
					str.Format("0x%X \r\n",AddressContents);
					m_StackDisplay += str;
					m_Status ="Received new bootloader script data";

					// Update displays
					UpdateData(FALSE);

					// Reschedule timer to ping again
					SetTimer(BL_SCRIPT_EXECUTION_TIMER,50,NULL);
				}
			// Reschedule timer to ping again
			} else {
				SetTimer(BL_SCRIPT_EXECUTION_TIMER,250,NULL);

				m_Status ="No new bootloader script data received";

				// Update displays
				UpdateData(FALSE);
			}
		// Command engine error detected, display error
		} else {
			commandEngine.GetErrorMsgString(m_Status);

			// Update displays
			UpdateData(FALSE);
		}
		break;
	}
	
	CPropertyPage::OnTimer(nIDEvent);
}

// Adds a new command at the end of a bootload script
void CScriptDlg::OnButtonaddcommand() 
{
	// get current position
	int nCurrentStackIndex = m_SpinStackIndex.GetPos();

	// If at end of stack and stack not full
	if( (nStackIndex == nCurrentStackIndex) && (nStackIndex < NUM_OF_STACK_ENTRIES) ) {

		ControlDataToStack(nCurrentStackIndex);

		// set control to point at end of stack
		nStackIndex++;
		m_SpinStackIndex.SetPos(nStackIndex);
	}

	RegenerateStackDisplay(nStackIndex);
}

// Inserts a new command after the current index of a bootload script
void CScriptDlg::OnButtoninsertcommand() 
{
	// get current position
	int nCurrentStackIndex = m_SpinStackIndex.GetPos();	

	// If not at end of stack, and stack not full with new entry
	if( (nCurrentStackIndex != nStackIndex) &&
		( (nStackIndex+1) < NUM_OF_STACK_ENTRIES) ) {
		
		// Stack is now one more larger
		nStackIndex++;

		// move all entries down one to make room for new entry
		for(int i=nStackIndex-1; i>nCurrentStackIndex; i--) {
			StackData[i] = StackData[i-1];
		}

		// copy info from controls to stack
		ControlDataToStack(nCurrentStackIndex);

		RegenerateStackDisplay(nCurrentStackIndex);
	}
}

// Over-writes command type and parameters from controls into the bootload script stack at the current index
void CScriptDlg::OnButtonupdatecommand() 
{
	// get current position
	int nCurrentStackIndex = m_SpinStackIndex.GetPos();

	// If not at end of stack
	if(nCurrentStackIndex != nStackIndex) {
		// copy info from controls to stack
		ControlDataToStack(nCurrentStackIndex);	

		RegenerateStackDisplay(nCurrentStackIndex);
	}
}

// Deletes the command and parameters from the bootload script stack at the current index
void CScriptDlg::OnButtonremovecommand() 
{
	// get current position
	int nCurrentStackIndex = m_SpinStackIndex.GetPos();

	// If not at end of stack
	if(nCurrentStackIndex != nStackIndex) {
		
		for(int i=nCurrentStackIndex; i<nStackIndex; i++) {
			// Move all entries up one
			StackData[i] = StackData[i+1];
		}

		if(nStackIndex > 0) {
			nStackIndex --;
		}

		if(nCurrentStackIndex != nStackIndex) {
			if( StackData[nCurrentStackIndex].uCommand < QUIT_CMD ) {
				m_ComboCommand.SetCurSel(StackData[nCurrentStackIndex].uCommand - 1);
			} else {
				m_ComboCommand.SetCurSel(StackData[nCurrentStackIndex].uCommand - 2);
			}
			SetUpScriptDataControls(StackData[nCurrentStackIndex].uCommand, true, nCurrentStackIndex);
		} else {
			m_ComboCommand.SetCurSel(0);
			SetUpScriptDataControls(PUSH_CMD,false,0);
		}

		RegenerateStackDisplay(nCurrentStackIndex);
	}
}

// Erases the entire bootload script
void CScriptDlg::OnButtonclearscript() 
{
	// Reset stack variables
	nStackIndex = 0;
	m_SpinStackIndex.SetPos(nStackIndex);
	SetUpScriptDataControls(PUSH_CMD,false,nStackIndex);
	

	// Zero out stack data structure
	for(int i=0; i<NUM_OF_STACK_ENTRIES; i++) {
		StackData[i].uCommand = 0;
		StackData[i].uParam1 = 0;
		StackData[i].uParam2 = 0;
		StackData[i].uParam3 = 0;
		StackData[i].uParam4 = 0;
		StackData[i].uParam5 = 0;
		StackData[i].uParam6 = 0;
		StackData[i].uParam7 = 0;
		StackData[i].uParam8 = 0;
	}

	// Initialize stack display
	RegenerateStackDisplay(nStackIndex);
}

// Sets up the parameter data for any selected command type, can copy parameters from bootload script stack into controls
void CScriptDlg::SetUpScriptDataControls(int nCmdIndex, bool bCopyStackData, int nStackDataIndex)
{
	CString str;

	// Default setup for controls
	m_EditParam1.SetWindowText("");
	m_EditParam2.SetWindowText("");
	m_EditParam3.SetWindowText("");
	m_EditParam4.SetWindowText("");
	m_EditParam5.SetWindowText("");
	m_EditParam6.SetWindowText("");
	m_EditParam7.SetWindowText("");
	m_EditParam8.SetWindowText("");

	m_EditParam1.EnableWindow(FALSE);
	m_EditParam2.EnableWindow(FALSE);
	m_EditParam3.EnableWindow(FALSE);
	m_EditParam4.EnableWindow(FALSE);
	m_EditParam5.EnableWindow(FALSE);
	m_EditParam6.EnableWindow(FALSE);
	m_EditParam7.EnableWindow(FALSE);
	m_EditParam8.EnableWindow(FALSE);

	m_EditParam1Hint.SetWindowText("");
	m_EditParam2Hint.SetWindowText("");
	m_EditParam3Hint.SetWindowText("");
	m_EditParam4Hint.SetWindowText("");
	m_EditParam5Hint.SetWindowText("");
	m_EditParam6Hint.SetWindowText("");
	m_EditParam7Hint.SetWindowText("");
	m_EditParam8Hint.SetWindowText("");

	switch(nCmdIndex) {

	// Push Stack Command: Param Count 1
	case PUSH_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam1Hint.SetWindowText("Data Word");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);
		}
		break;
	
	// Read Command: Param Count 2
	case READ_XDATA_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam2.EnableWindow(TRUE);

		m_EditParam1Hint.SetWindowText("Addr Hi");
		m_EditParam2Hint.SetWindowText("Addr Lo");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam2);
			m_EditParam2.SetWindowText(str);
		}
		break;

	// Write Command: Param Count 3
	case WRITE_XDATA_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam2.EnableWindow(TRUE);
		m_EditParam3.EnableWindow(TRUE);

		m_EditParam1Hint.SetWindowText("Addr Hi");
		m_EditParam2Hint.SetWindowText("Addr Lo");
		m_EditParam3Hint.SetWindowText("Data Word");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam2);
			m_EditParam2.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam3);
			m_EditParam3.SetWindowText(str);
		}
		break;

	// Read Command: Param Count 1
	case READ_IDATA_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam1Hint.SetWindowText("Addr");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);
		}
		break;

	// Write Command: Param Count 2
	case WRITE_IDATA_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam2.EnableWindow(TRUE);

		m_EditParam1Hint.SetWindowText("Addr Hi");
		m_EditParam2Hint.SetWindowText("Data Word");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam2);
			m_EditParam2.SetWindowText(str);
		}
		break;

	// Read Command: Param Count 1
	case READ_SFR_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam1Hint.SetWindowText("Addr");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);
		}
		break;

	// Write Command: Param Count 2
	case WRITE_SFR_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam2.EnableWindow(TRUE);

		m_EditParam1Hint.SetWindowText("Addr Hi");
		m_EditParam2Hint.SetWindowText("Data Word");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam2);
			m_EditParam2.SetWindowText(str);
		}
		break;


	// Delay Command: Param Count 1
	case DELAY_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam1Hint.SetWindowText("Value MS");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);
		}
		break;

	// Repeat Command: Param Count 1
	case REPEAT_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam1Hint.SetWindowText("Loop Count");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);
		}
		break;

	// Write then Read XDATA Command: Param Count 6
	case WR_RD_XDATA_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam2.EnableWindow(TRUE);
		m_EditParam3.EnableWindow(TRUE);
		m_EditParam4.EnableWindow(TRUE);
		m_EditParam5.EnableWindow(TRUE);
		m_EditParam6.EnableWindow(TRUE);

		m_EditParam1Hint.SetWindowText("Wr Addr Hi");
		m_EditParam2Hint.SetWindowText("Wr Addr Lo");
		m_EditParam3Hint.SetWindowText("Wr Data Word");
		m_EditParam4Hint.SetWindowText("Rd Addr Hi");
		m_EditParam5Hint.SetWindowText("Rd Addr Lo");
		m_EditParam6Hint.SetWindowText("Rd Count");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam2);
			m_EditParam2.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam3);
			m_EditParam3.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam4);
			m_EditParam4.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam5);
			m_EditParam5.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam6);
			m_EditParam6.SetWindowText(str);
		}
		break;

	// Pop data from stack and write to mem loc Command: Param Count 2
	case POP_TO_XDATA_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam2.EnableWindow(TRUE);

		m_EditParam1Hint.SetWindowText("Addr Hi");
		m_EditParam2Hint.SetWindowText("Addr Lo");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam2);
			m_EditParam2.SetWindowText(str);
		}
		break;

	// write to mem loc then write to mem loc Command: Param Count 6
	case WR_WR_XDATA_CMD:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam2.EnableWindow(TRUE);
		m_EditParam3.EnableWindow(TRUE);
		m_EditParam4.EnableWindow(TRUE);
		m_EditParam5.EnableWindow(TRUE);
		m_EditParam6.EnableWindow(TRUE);

		m_EditParam1Hint.SetWindowText("Wr#1 AddrHi");
		m_EditParam2Hint.SetWindowText("Wr#1 Addr Lo");
		m_EditParam3Hint.SetWindowText("#1 Data Word");
		m_EditParam4Hint.SetWindowText("Wr#2 Addr Hi");
		m_EditParam5Hint.SetWindowText("Wr#2 Addr Lo");
		m_EditParam6Hint.SetWindowText("#2 Data Word");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam2);
			m_EditParam2.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam3);
			m_EditParam3.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam4);
			m_EditParam4.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam5);
			m_EditParam5.SetWindowText(str);

			str.Format("0x%X",StackData[nStackDataIndex].uParam6);
			m_EditParam6.SetWindowText(str);
		}
		break;

	// Sign Extend Data Stack for A2D Conversion Count Operations
	case SIGN_EXTEND_DATA_STACK:
		m_EditParam1.EnableWindow(TRUE);
		m_EditParam1Hint.SetWindowText("Sign Bit 0-15");

		if(bCopyStackData) {
			str.Format("0x%X",StackData[nStackDataIndex].uParam1);
			m_EditParam1.SetWindowText(str);
		}
		break;
    }
}

// Used to transfer control type and parameters data into the bootload script stack
void CScriptDlg::ControlDataToStack(int nIndex)
{
	CString str;

	// copy info from controls to stack
	switch (m_ComboCommand.GetCurSel() + 1) {
	case PUSH_CMD:
		StackData[nIndex].uCommand = PUSH_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case READ_XDATA_CMD:
		StackData[nIndex].uCommand = READ_XDATA_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam2.GetWindowText(str);
		StackData[nIndex].uParam2 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case WRITE_XDATA_CMD:
		StackData[nIndex].uCommand = WRITE_XDATA_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam2.GetWindowText(str);
		StackData[nIndex].uParam2 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam3.GetWindowText(str);
		StackData[nIndex].uParam3 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case READ_IDATA_CMD:
		StackData[nIndex].uCommand = READ_IDATA_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case WRITE_IDATA_CMD:
		StackData[nIndex].uCommand = WRITE_IDATA_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam2.GetWindowText(str);
		StackData[nIndex].uParam2 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case READ_SFR_CMD:
		StackData[nIndex].uCommand = READ_SFR_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case WRITE_SFR_CMD:
		StackData[nIndex].uCommand = WRITE_SFR_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam2.GetWindowText(str);
		StackData[nIndex].uParam2 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case DELAY_CMD:
		StackData[nIndex].uCommand = DELAY_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case REPEAT_CMD:
		StackData[nIndex].uCommand = REPEAT_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case WR_RD_XDATA_CMD:
		StackData[nIndex].uCommand = WR_RD_XDATA_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam2.GetWindowText(str);
		StackData[nIndex].uParam2 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam3.GetWindowText(str);
		StackData[nIndex].uParam3 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam4.GetWindowText(str);
		StackData[nIndex].uParam4 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam5.GetWindowText(str);
		StackData[nIndex].uParam5 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam6.GetWindowText(str);
		StackData[nIndex].uParam6 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case POP_TO_XDATA_CMD:
		StackData[nIndex].uCommand = POP_TO_XDATA_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam2.GetWindowText(str);
		StackData[nIndex].uParam2 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case WR_WR_XDATA_CMD:
		StackData[nIndex].uCommand = WR_WR_XDATA_CMD;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam2.GetWindowText(str);
		StackData[nIndex].uParam2 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam3.GetWindowText(str);
		StackData[nIndex].uParam3 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam4.GetWindowText(str);
		StackData[nIndex].uParam4 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam5.GetWindowText(str);
		StackData[nIndex].uParam5 = static_cast<WORD>(strtol(str, NULL, 16));

		m_EditParam6.GetWindowText(str);
		StackData[nIndex].uParam6 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	case SIGN_EXTEND_DATA_STACK:
		StackData[nIndex].uCommand = SIGN_EXTEND_DATA_STACK;

		m_EditParam1.GetWindowText(str);
		StackData[nIndex].uParam1 = static_cast<WORD>(strtol(str, NULL, 16));
		break;

	// No Parameters
	default:
		if( (m_ComboCommand.GetCurSel() + 1) < QUIT_CMD) {
			StackData[nIndex].uCommand = m_ComboCommand.GetCurSel() + 1;
		} else {
			StackData[nIndex].uCommand = m_ComboCommand.GetCurSel() + 2;
		}
	}
}

// Used to format the stack display, current index selected is highlighted.
void CScriptDlg::RegenerateStackDisplay(int nCurrentStackIndex, bool bIsForFile)
{
	bool bIndent = false;
	int nPos = 0;
	int nLastPos = 0;
	CString str;

	m_StackDisplay = "";

	// Loop through all entries, create display line, and add line to stack listing in edit box
	for(int i=0; i<nStackIndex; i++) {

		if( bIsForFile ) {
			m_StackDisplay += "<!-- ";
		}

		if( (bIndent) &&
			(StackData[i].uCommand != REPEAT_CMD) &&
			(StackData[i].uCommand != WHILE_CMD) &&
			(StackData[i].uCommand != IF_CMD) &&
			(StackData[i].uCommand != END_REPEAT_CMD) &&
			(StackData[i].uCommand != END_WHILE_CMD) &&
			(StackData[i].uCommand != END_IF_CMD)          )
		{
			m_StackDisplay += "    ";
		}

		// Record character position of Stack Display so we can highlight or select the active index
		if(i == nCurrentStackIndex) {
			nLastPos = m_StackDisplay.GetLength();
		}
		
		switch (StackData[i].uCommand) {
		case PUSH_CMD:
			str.Format("[%2d] => PUSH [0x%X] [0x%X]  \r\n",i,StackData[i].uCommand, StackData[i].uParam1);
			m_StackDisplay += str;
			break;

		case DUP_CMD:
			str.Format("[%2d] => DUP [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case SWAP_CMD:
			str.Format("[%2d] => SWAP [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case POP_CMD:
			str.Format("[%2d] => POP [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case XMIT_CMD:
			str.Format("[%2d] => TRANSMIT [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case READ_XDATA_CMD:
			str.Format("[%2d] => RD XDATA [0x%X] [0x%X] [0x%X]  \r\n",
					i,
					StackData[i].uCommand,
					StackData[i].uParam1,
					StackData[i].uParam2);
			m_StackDisplay += str;
			break;

		case WRITE_XDATA_CMD:
			str.Format("[%2d] => WR XDATA [0x%X] [0x%X] [0x%X] [0x%X]  \r\n",
					i,
					StackData[i].uCommand,
					StackData[i].uParam1,
					StackData[i].uParam2,
					StackData[i].uParam3);
			m_StackDisplay += str;
			break;

		case READ_IDATA_CMD:
			str.Format("[%2d] => WR IDATA [0x%X] [0x%X]  \r\n",
					i,
					StackData[i].uCommand,
					StackData[i].uParam1);
			m_StackDisplay += str;
			break;

		case WRITE_IDATA_CMD:
			str.Format("[%2d] => WR IDATA [0x%X] [0x%X] [0x%X]  \r\n",
					i,
					StackData[i].uCommand,
					StackData[i].uParam1,
					StackData[i].uParam2);
			m_StackDisplay += str;
			break;

		case READ_SFR_CMD:
			str.Format("[%2d] => RD SFR [0x%X] [0x%X]  \r\n",
					i,
					StackData[i].uCommand,
					StackData[i].uParam1);
			m_StackDisplay += str;
			break;

		case WRITE_SFR_CMD:
			str.Format("[%2d] => WR SFR [0x%X] [0x%X] [0x%X]  \r\n",
					i,
					StackData[i].uCommand,
					StackData[i].uParam1,
					StackData[i].uParam2);
			m_StackDisplay += str;
			break;

		case EQUAL_CMD:
			str.Format("[%2d] => '=' [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case NOT_EQUAL_CMD:
			str.Format("[%2d] => '!=' [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case GREATER_THAN_OR_EQUAL_CMD:
			str.Format("[%2d] => '>=' [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case GREATER_THAN_CMD:
			str.Format("[%2d] => '>' [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case LESS_THAN_OR_EQUAL_CMD:
			str.Format("[%2d] => '<=' [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case LESS_THAN_CMD:
			str.Format("[%2d] => '<' [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case LOGICAL_AND_CMD:
			str.Format("[%2d] => LOG AND [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case LOGICAL_OR_CMD:
			str.Format("[%2d] => LOG OR [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case LOGICAL_COMPLEMENT_CMD:
			str.Format("[%2d] => LOG ! [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case BITWISE_AND_CMD:
			str.Format("[%2d] => BIT AND [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case BITWISE_OR_CMD:
			str.Format("[%2d] => BIT OR [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case BITWISE_COMPLEMENT_CMD:
			str.Format("[%2d] => BIT ! [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case SHIFT_LEFT_CMD:
			str.Format("[%2d] => 'SFT <<' [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case SHIFT_RIGHT_CMD:
			str.Format("[%2d] => 'SFT >>' [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case ADD_CMD:
			str.Format("[%2d] => ADD [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case SUBTRACT_CMD:
			str.Format("[%2d] => SUB [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case REPEAT_CMD:
			bIndent = true;
			str.Format("[%2d] => REPEAT [0x%X] [0x%X]  \r\n",
					i,
					StackData[i].uCommand,
					StackData[i].uParam1);
			m_StackDisplay += str;
			break;

		case END_REPEAT_CMD:
			bIndent = false;
			str.Format("[%2d] => END REPEAT [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case WHILE_CMD:
			bIndent = true;
			str.Format("[%2d] => WHILE [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case END_WHILE_CMD:
			bIndent = false;
			str.Format("[%2d] => END WHILE [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case IF_CMD:
			bIndent = true;
			str.Format("[%2d] => IF [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case END_IF_CMD:
			bIndent = false;
			str.Format("[%2d] => END IF [0x%X]\r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
			break;

		case DELAY_CMD:
			str.Format("[%2d] => DELAY [0x%X] [0x%X]  \r\n",i,StackData[i].uCommand,StackData[i].uParam1);
			m_StackDisplay += str;
			break;

		case WR_RD_XDATA_CMD:
			str.Format("[%2d] => WR RD [0x%X] [0x%X] [0x%X] [0x%X] [0x%X] [0x%X] [0x%X] \r\n",
					i,
					StackData[i].uCommand,
					StackData[i].uParam1,
					StackData[i].uParam2,
					StackData[i].uParam3,
					StackData[i].uParam4,
					StackData[i].uParam5,
					StackData[i].uParam6);
			m_StackDisplay += str;
			break;

		case POP_TO_XDATA_CMD:
			str.Format("[%2d] => POP2 ADDR [0x%X] [0x%X] [0x%X]  \r\n",
					i,
					StackData[i].uCommand,
					StackData[i].uParam1,
					StackData[i].uParam2);
			m_StackDisplay += str;
			break;

		case WR_WR_XDATA_CMD:
			str.Format("[%2d] => WR WR [0x%X] [0x%X] [0x%X] [0x%X] [0x%X] [0x%X] [0x%X] \r\n",
					i,
					StackData[i].uCommand,
					StackData[i].uParam1,
					StackData[i].uParam2,
					StackData[i].uParam3,
					StackData[i].uParam4,
					StackData[i].uParam5,
					StackData[i].uParam6);
			m_StackDisplay += str;
			break;

   		case NOP_CMD:
            str.Format("[%2d] => NOP [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;

		case RESET_DATA_STACK:
			str.Format("[%2d] => RESET STACK [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;

		case PUSH_MAX_VALUE_OF_DATA_STACK:
			str.Format("[%2d] => STACK MAX VAL [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;

		case PUSH_MIN_VALUE_OF_DATA_STACK:
			str.Format("[%2d] => STACK MIN VAL [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;

		case PUSH_SUM_OF_DATA_STACK:
			str.Format("[%2d] => STACK SUM [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;

		case SIGN_EXTEND_DATA_STACK:
			str.Format("[%2d] => SIGN EXTEND [0x%X] [0x%X]  \r\n",i,StackData[i].uCommand,StackData[i].uParam1);
			m_StackDisplay += str;
			break;

		case ABSOLUTE_VALUE_DATA_STACK:
			str.Format("[%2d] => STACK ABS VAL [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;

		case READ_XDATA2_CMD:
			str.Format("[%2d] => RD XDATA 2 [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;

		case WRITE_XDATA2_CMD:
			str.Format("[%2d] => WR XDATA 2 [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;

		case READ_IDATA2_CMD:
			str.Format("[%2d] => RD IDATA 2 [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;

		case WRITE_IDATA2_CMD:
			str.Format("[%2d] => WR IDATA 2 [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;

		case READ_SFR2_CMD:
			str.Format("[%2d] => RD SFR 2 [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;

		case WRITE_SFR2_CMD:
			str.Format("[%2d] => WR SFR 2 [0x%X] \r\n",i,StackData[i].uCommand);
			m_StackDisplay += str;
            break;
		}

		// Record character position of Stack Display so we can highlight or select the active index
		if(i == nCurrentStackIndex) {
			nPos = m_StackDisplay.GetLength();
		}

		if( bIsForFile ) {
			m_StackDisplay.TrimRight("\r\n");
			m_StackDisplay += " -->\r\n";
		}
	}
	// Put something at the end of display
	if(nCurrentStackIndex == nStackIndex) {
		nLastPos = m_StackDisplay.GetLength();
	}

	if( !bIsForFile ) {
		str.Format("[%2d] => ADD NEW COMMAND  \r\n",nStackIndex);
		m_StackDisplay += str;
	}

	if(nCurrentStackIndex == nStackIndex) {
		nPos = m_StackDisplay.GetLength();
	}

	CEdit *pEdit = (CEdit *)GetDlgItem(IDC_EDIT_BLSCRIPT);

	// Highlight or select Current Index 
	pEdit->SetSel(nLastPos,nPos);
	UpdateData(FALSE);

	// Scroll the edit box to the last position for new entries
	pEdit->LineScroll(nStackIndex -1);

	// Setup controls based upon the position of current index
	CButton* pBtn[9];
	pBtn[0] = (CButton *)GetDlgItem(IDC_BUTTONINSERTCOMMAND);
	pBtn[1] = (CButton *)GetDlgItem(IDC_BUTTONADDCOMMAND);
	pBtn[2] = (CButton *)GetDlgItem(IDC_BUTTONREMOVECOMMAND);
	pBtn[3] = (CButton *)GetDlgItem(IDC_BUTTONCLEARSCRIPT);
	pBtn[4] = (CButton *)GetDlgItem(IDC_BUTTONUPDATECOMMAND);

	pBtn[5] = (CButton *)GetDlgItem(IDC_BUTTONSAVESCRIPT);
	pBtn[6] = (CButton *)GetDlgItem(IDC_BUTTONSENDSCRIPT);
	pBtn[7] = (CButton *)GetDlgItem(IDC_BUTTONRUNBLSCRIPT);

	// Enable All to start with
	for(int x=0; x<8; x++) {
		pBtn[x]->EnableWindow(TRUE);
	}

	// Disable Clear, Save, Send, and Run buttons if there is no stack data
	if(nStackIndex == 0 ) {
		pBtn[3]->EnableWindow(FALSE);
		pBtn[5]->EnableWindow(FALSE);
		pBtn[6]->EnableWindow(FALSE);
		pBtn[7]->EnableWindow(FALSE);
	}

	// Current Index at Bottom of list or empty (Add Only)
	if( (nCurrentStackIndex == nStackIndex) || (nStackIndex == 0) )
	{
		pBtn[0]->EnableWindow(FALSE);
		pBtn[2]->EnableWindow(FALSE);
		pBtn[4]->EnableWindow(FALSE);
	}
	// Can't add if anywhere else
	else
	{
		pBtn[1]->EnableWindow(FALSE);
	}
}

// Transfers bootload stack data structure data into a linear word buffer for transmission to IUADP
int CScriptDlg::StackToCommBuffer()
{
	int nNumWords = 0;
	
	// Loop through all entries, copy to comm buffer for commandEngine
	for(int i=0; (i < nStackIndex) && (nNumWords < (SCRIPT_BUFFER_SIZE - 1)); i++) {
		
		switch (StackData[i].uCommand) {
		case PUSH_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			break;

		case READ_XDATA_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			ComData[nNumWords++] = StackData[i].uParam2;
			break;

		case WRITE_XDATA_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			ComData[nNumWords++] = StackData[i].uParam2;
			ComData[nNumWords++] = StackData[i].uParam3;
			break;

		case READ_IDATA_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			break;

		case WRITE_IDATA_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			ComData[nNumWords++] = StackData[i].uParam2;
			break;

		case READ_SFR_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			break;

		case WRITE_SFR_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			ComData[nNumWords++] = StackData[i].uParam2;
			break;

		case REPEAT_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			break;

		case DELAY_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			break;
		
		case WR_RD_XDATA_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			ComData[nNumWords++] = StackData[i].uParam2;
			ComData[nNumWords++] = StackData[i].uParam3;
			ComData[nNumWords++] = StackData[i].uParam4;
			ComData[nNumWords++] = StackData[i].uParam5;
			ComData[nNumWords++] = StackData[i].uParam6;
			break;

		case POP_TO_XDATA_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			ComData[nNumWords++] = StackData[i].uParam2;
			break;

		case WR_WR_XDATA_CMD:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			ComData[nNumWords++] = StackData[i].uParam2;
			ComData[nNumWords++] = StackData[i].uParam3;
			ComData[nNumWords++] = StackData[i].uParam4;
			ComData[nNumWords++] = StackData[i].uParam5;
			ComData[nNumWords++] = StackData[i].uParam6;
            break;

		case SIGN_EXTEND_DATA_STACK:
			ComData[nNumWords++] = StackData[i].uCommand;
			ComData[nNumWords++] = StackData[i].uParam1;
			break;
        
		default:
			ComData[nNumWords++] = StackData[i].uCommand;
		}
	}

	// Add quit command to end of buffer
	ComData[nNumWords++] = QUIT_CMD;

	return nNumWords;
}

// Used to send script data to IUADP using block type Poke Command
void CScriptDlg::OnButtonsendscript() 
{
	CString str;

	// If advanced bootloader is available, set the memory area first
	bErrorDetected = false;
	if(bAdvancedBootloaderAvailable) {
		MemoryArea ma = DisableAll;

		if (!commandEngine.SelectMemory(ma)) {
        	commandEngine.GetErrorMsgString(m_Status);
        	bErrorDetected = true;
		}
	}

	// Copy stack data into a com buffer to transmission to IUADP
	int nNumWords = StackToCommBuffer();

	// Send Script Data To PIU in 32 word blocks
	unsigned int uLoadAddress = 0x1000;
	int nNumSentWords = 0;
	int nNumWordsThisBlock = 32;

	while( !bErrorDetected && (nNumSentWords < nNumWords) && (nNumSentWords < SCRIPT_BUFFER_SIZE) )
	{
		// Handle a block size of less than 32 words
		if( (nNumWords - nNumSentWords) < 32) {
			nNumWordsThisBlock = nNumWords - nNumSentWords;
		}

		if(!commandEngine.ScriptLoad(uLoadAddress,&ComData[nNumSentWords],nNumWordsThisBlock)) {
			// Report Error status
			commandEngine.GetErrorMsgString(str);
			m_Status = str;

			// Set error flag to stop loading script data to IUADP
			bErrorDetected = true;
		} else {
			// Increment load address for next block
			uLoadAddress += nNumWordsThisBlock * 2;

			// Increment number of words sent to IUADP
			nNumSentWords += nNumWordsThisBlock;
		}
	}

	if(!bErrorDetected) {
		m_Status.Format("Successfully loaded %d words of script data into the IUADP \r\n",nNumWords);
	}

/*
	if(!bErrorDetected) {
		// Send Script Data To PIU
		if(commandEngine.ScriptLoad(0x1000,ComData,nNumWords)) {
			m_Status = "Script successfully loaded into IUADP \r\n";
		} else {
			// Update status
			commandEngine.GetErrorMsgString(str);
			m_Status = str;
		}
	}
*/
	// Update controls
	UpdateData(FALSE);	
}

// Used to command IUADP to run loaded bootloader script data, starts timer to poll for script completion or data
void CScriptDlg::OnButtonrunblscript() 
{
	m_StackDisplay = "Displaying Received Data \r\n";

	// Disable the run button until script if finished or stoped
	CButton * pBtn = (CButton *)GetDlgItem(IDC_BUTTONRUNBLSCRIPT);
	pBtn->EnableWindow(FALSE);

	// Enable the stop data monitoring button
	pBtn = (CButton *)GetDlgItem(IDC_BUTTONSTOPDATAMONITOR);
	pBtn->EnableWindow(TRUE);

	// Start Timer to Execute Script
	SetTimer(BL_SCRIPT_EXECUTION_TIMER, 250, NULL);

	// Update controls
	UpdateData(FALSE);
}

// Used to stop bootloader script data and completion monitor
void CScriptDlg::OnButtonstopdatamonitor() 
{
	// Stop any previous com port monitoring
	KillTimer(BL_SCRIPT_EXECUTION_TIMER);	

	// Enable Run button
	CButton * pBtn = (CButton *)GetDlgItem(IDC_BUTTONRUNBLSCRIPT);
	pBtn->EnableWindow(TRUE);

	// Disable the stop data monitor button
	pBtn = (CButton *)GetDlgItem(IDC_BUTTONSTOPDATAMONITOR);
	pBtn->EnableWindow(FALSE);

	RegenerateStackDisplay(nStackIndex);
}

// Used to setup bootload script controls when user picks a different type of command
void CScriptDlg::OnSelchangeComboactions() 
{
	SetUpScriptDataControls(m_ComboCommand.GetCurSel() + 1,false,0);
}

// Used to setup dialog when user selects Scripting dialog
BOOL CScriptDlg::OnSetActive() 
{
	// Point to bottom of listing
	m_SpinStackIndex.SetPos(nStackIndex);
	
	// Setup to select the first available action
	m_ComboCommand.SetCurSel(0);
	SetUpScriptDataControls(PUSH_CMD,false,0);

	(GetDlgItem(IDC_SPINSCRIPTINDEX))->EnableWindow(bAdvancedBootloaderAvailable);
	(GetDlgItem(IDC_BUTTONLOADSCRIPT))->EnableWindow(bAdvancedBootloaderAvailable);
	(GetDlgItem(IDC_BUTTONINSERTCOMMAND))->EnableWindow(bAdvancedBootloaderAvailable);
	(GetDlgItem(IDC_BUTTONADDCOMMAND))->EnableWindow(bAdvancedBootloaderAvailable);
	(GetDlgItem(IDC_BUTTONREMOVECOMMAND))->EnableWindow(bAdvancedBootloaderAvailable);
	(GetDlgItem(IDC_BUTTONCLEARSCRIPT))->EnableWindow(bAdvancedBootloaderAvailable);
	(GetDlgItem(IDC_BUTTONUPDATECOMMAND))->EnableWindow(bAdvancedBootloaderAvailable);
	(GetDlgItem(IDC_BUTTONRUNBLSCRIPT))->EnableWindow(bAdvancedBootloaderAvailable);
	(GetDlgItem(IDC_BUTTONSAVESCRIPT))->EnableWindow(bAdvancedBootloaderAvailable);
	(GetDlgItem(IDC_BUTTONSENDSCRIPT))->EnableWindow(bAdvancedBootloaderAvailable);
	(GetDlgItem(IDC_EDITCMDINDEX))->EnableWindow(bAdvancedBootloaderAvailable);
	m_ComboCommand.EnableWindow(bAdvancedBootloaderAvailable);

	(GetDlgItem(IDC_BUTTONSTOPDATAMONITOR))->EnableWindow(FALSE);

	// Regenerate the display
	if(bAdvancedBootloaderAvailable) {
		RegenerateStackDisplay(nStackIndex);
	}

	return CPropertyPage::OnSetActive();
}

// Handles indexing into each element of the bootload script
void CScriptDlg::OnDeltaposSpinscriptindex(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;

	CString str = "";;
	int nNewPos = pNMUpDown->iPos + pNMUpDown->iDelta;

	if( (nNewPos >= 0) && (nNewPos < nStackIndex) ) {

		// Fill in parameter fields and set combo box
		if( StackData[nNewPos].uCommand < QUIT_CMD ) {
			m_ComboCommand.SetCurSel(StackData[nNewPos].uCommand - 1);
		} else {
			m_ComboCommand.SetCurSel(StackData[nNewPos].uCommand - 2);
		}
		SetUpScriptDataControls(StackData[nNewPos].uCommand, true, nNewPos);

		// Accept change in position
		*pResult = 0;

		RegenerateStackDisplay(nNewPos);
	} else if(nNewPos == nStackIndex) {

		m_ComboCommand.SetCurSel(0);
		SetUpScriptDataControls(PUSH_CMD, false, 0);

		// Accept change in position
		*pResult = 0;

		RegenerateStackDisplay(nNewPos);
	} else {

		// Reject change in position
		*pResult = 1;
	}
}

// Used to save a bootloader script to disk, also saves a raw binary file
void CScriptDlg::OnButtonsavescript() 
{
	CString filename;
	int nCurrentStackIndex = 0;
	// Launch the file open dialog to get file name

	static char BASED_CODE szFilter[] = "Bootload Script Files (*.dat)|*.dat|All Files (*.*)|*.*|";
	CFileDialog dlgFile(FALSE,NULL,NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter);
	if(dlgFile.DoModal() == IDOK)
	{
		filename = dlgFile.GetPathName();

		int nPos = filename.Find(".",0);

		if(nPos != -1) {
			filename.Delete(nPos,filename.GetLength() - nPos);
		}

		using namespace std;

		std::ofstream blscriptfile;

		// Save stack file
		blscriptfile.open(filename + ".dat", ios::out | ios::binary | ios::trunc);

		if( blscriptfile.is_open() ) {
			// Write out size of stack
			blscriptfile.write(reinterpret_cast<char *>(&nStackIndex),sizeof(nStackIndex));

			// Write out stack
			while( !blscriptfile.eof() && nCurrentStackIndex < nStackIndex) {
				blscriptfile.write(reinterpret_cast<char *>(&StackData[nCurrentStackIndex]),sizeof(StackData[nCurrentStackIndex]));
				nCurrentStackIndex++;
			}

			blscriptfile.close();

			SetDlgItemText(IDC_EDITSCRIPTFILENAME, filename + ".dat");
		}

		// Save binary script file for XML
		blscriptfile.open(filename + ".bin", ios::out | ios::binary | ios::trunc);
		int nNumOfWords = StackToCommBuffer();

		if( blscriptfile.is_open() ) {
			// Write out comm buffer
			blscriptfile.write(reinterpret_cast<char *>(&ComData),sizeof(WORD)*nNumOfWords);

			blscriptfile.close();
		}

		// Save text listing of script file for XML
		blscriptfile.open(filename + ".txt");

		if( blscriptfile.is_open() ) {
			// Regenerate stack with XML comment symbols
			RegenerateStackDisplay(nStackIndex, true);

			// Make header
			if( !blscriptfile.eof() ) {
				CString str = "<!-- ";
				str += filename;
				str += ".bin";
				str += " BOOTLOADER SCRIPT -->\r\n";
				blscriptfile.write(reinterpret_cast<const char *>((LPCTSTR)str),str.GetLength());
			}

			// Write out stack
			if( !blscriptfile.eof() ) {
				blscriptfile.write(reinterpret_cast<const char *>((LPCTSTR)m_StackDisplay),m_StackDisplay.GetLength());
			}

			blscriptfile.close();

			// Regenerate stack without XML comment symbols
			RegenerateStackDisplay(nStackIndex);
		}
	}
}

// Used to load a save bootload script from disk
void CScriptDlg::OnButtonloadscript() 
{
	CString filename;
	int nCurrentStackIndex = 0;

	// Launch the file open dialog to get file name

	static char BASED_CODE szFilter[] = "Bootload Script Files (*.dat)|*.dat|All Files (*.*)|*.*|";
	CFileDialog dlgFile(TRUE,NULL,NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter);
	if(dlgFile.DoModal() == IDOK)
	{
		filename = dlgFile.GetPathName();

		using namespace std;
		
		std::ifstream blscriptfile;
		blscriptfile.open(filename,ios_base::in | ios_base::binary);

		if( blscriptfile.is_open() ) {
			// Read in size of stack
			blscriptfile.read(reinterpret_cast<char *>(&nStackIndex),sizeof(nStackIndex));

			// Read in stack
			while( !blscriptfile.eof() && nCurrentStackIndex < nStackIndex) {
				blscriptfile.read(reinterpret_cast<char *>(&StackData[nCurrentStackIndex]),sizeof(StackData[nCurrentStackIndex]));
				nCurrentStackIndex++;
			}

			m_ComboCommand.SetCurSel(0);
			nCurrentStackIndex = nStackIndex;
			m_SpinStackIndex.SetPos(nCurrentStackIndex);

			RegenerateStackDisplay(nStackIndex);

			// Display file name loaded and scroll to the right
			SetDlgItemText(IDC_EDITSCRIPTFILENAME, filename);
			CEdit *pEdit = reinterpret_cast<CEdit *>(GetDlgItem(IDC_EDITSCRIPTFILENAME));
			int nLength = pEdit->LineLength();
			pEdit->SetSel(nLength, nLength,TRUE);
		}
	}
}

BOOL CScriptDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	for(int i=0; i<NUM_OF_STACK_ENTRIES; i++) {
		StackData[i].uCommand = 0;
		StackData[i].uParam1 = 0;
		StackData[i].uParam2 = 0;
		StackData[i].uParam3 = 0;
		StackData[i].uParam4 = 0;
		StackData[i].uParam5 = 0;
		StackData[i].uParam6 = 0;
		StackData[i].uParam7 = 0;
		StackData[i].uParam8 = 0;
	}

	// Reset bootloader stack variables
	nStackIndex = 0;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}