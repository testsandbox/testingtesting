#if !defined(AFX_MemAccessDlg_H__E3389355_BB89_45E5_95BF_32E201F48D59__INCLUDED_)
#define AFX_MemAccessDlg_H__E3389355_BB89_45E5_95BF_32E201F48D59__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MemAccessDlg.h : header file
//

#include "Globals.h"

#define MAI_NONE	(0)
#define MAI_SFR		(1)
#define MAI_CPU_RAM (2)
#define MAI_CPU_ROM (3)

#define PEEKPOKE_LOOP_TIMER		500
#define TREE_LOOP_TIMER			600

#define NUM_CONTROL_GROUPS		18

// Control IDs for Read Write Memory Controls
#define IDC_MEMTYPECTL1		2000
#define IDC_MEMADDRCTL1		2001
#define IDC_MEMVALUECTL1	2002
#define IDC_MEMREADBTN1		2003
#define IDC_MEMWRITEBTN1	2004
#define IDC_MEMRDENBTN1		2005
#define IDC_MEMWRENBTN1		2006

#define IDC_MEMTYPECTL2		2007
#define IDC_MEMADDRCTL2		2008
#define IDC_MEMVALUECTL2	2009
#define IDC_MEMREADBTN2		2010
#define IDC_MEMWRITEBTN2	2011
#define IDC_MEMRDENBTN2		2012
#define IDC_MEMWRENBTN2		2013

#define IDC_MEMTYPECTL3		2014
#define IDC_MEMADDRCTL3		2015
#define IDC_MEMVALUECTL3	2016
#define IDC_MEMREADBTN3		2017
#define IDC_MEMWRITEBTN3	2018
#define IDC_MEMRDENBTN3		2019
#define IDC_MEMWRENBTN3		2020

#define IDC_MEMTYPECTL4		2021
#define IDC_MEMADDRCTL4		2022
#define IDC_MEMVALUECTL4	2023
#define IDC_MEMREADBTN4		2024
#define IDC_MEMWRITEBTN4	2025
#define IDC_MEMRDENBTN4		2026
#define IDC_MEMWRENBTN4		2027

#define IDC_MEMTYPECTL5		2028
#define IDC_MEMADDRCTL5		2029
#define IDC_MEMVALUECTL5	2030
#define IDC_MEMREADBTN5		2031
#define IDC_MEMWRITEBTN5	2032
#define IDC_MEMRDENBTN5		2033
#define IDC_MEMWRENBTN5		2034

#define IDC_MEMTYPECTL6		2035
#define IDC_MEMADDRCTL6		2036
#define IDC_MEMVALUECTL6	2037
#define IDC_MEMREADBTN6		2038
#define IDC_MEMWRITEBTN6	2039
#define IDC_MEMRDENBTN6		2040
#define IDC_MEMWRENBTN6		2041

#define IDC_MEMTYPECTL7		2042
#define IDC_MEMADDRCTL7		2043
#define IDC_MEMVALUECTL7	2044
#define IDC_MEMREADBTN7		2045
#define IDC_MEMWRITEBTN7	2046
#define IDC_MEMRDENBTN7		2047
#define IDC_MEMWRENBTN7		2048

#define IDC_MEMTYPECTL8		2049
#define IDC_MEMADDRCTL8		2050
#define IDC_MEMVALUECTL8	2051
#define IDC_MEMREADBTN8		2052
#define IDC_MEMWRITEBTN8	2053
#define IDC_MEMRDENBTN8		2054
#define IDC_MEMWRENBTN8		2055

#define IDC_MEMTYPECTL9		2056
#define IDC_MEMADDRCTL9		2057
#define IDC_MEMVALUECTL9	2058
#define IDC_MEMREADBTN9		2059
#define IDC_MEMWRITEBTN9	2060
#define IDC_MEMRDENBTN9		2061
#define IDC_MEMWRENBTN9		2062

#define IDC_MEMTYPECTL10	2063
#define IDC_MEMADDRCTL10	2064
#define IDC_MEMVALUECTL10	2065
#define IDC_MEMREADBTN10	2066
#define IDC_MEMWRITEBTN10	2067
#define IDC_MEMRDENBTN10	2068
#define IDC_MEMWRENBTN10	2069

#define IDC_MEMTYPECTL11	2070
#define IDC_MEMADDRCTL11	2071
#define IDC_MEMVALUECTL11	2072
#define IDC_MEMREADBTN11	2073
#define IDC_MEMWRITEBTN11	2074
#define IDC_MEMRDENBTN11	2075
#define IDC_MEMWRENBTN11	2076

#define IDC_MEMTYPECTL12	2077
#define IDC_MEMADDRCTL12	2078
#define IDC_MEMVALUECTL12	2079
#define IDC_MEMREADBTN12	2080
#define IDC_MEMWRITEBTN12	2081
#define IDC_MEMRDENBTN12	2082
#define IDC_MEMWRENBTN12	2083

#define IDC_MEMTYPECTL13	2084
#define IDC_MEMADDRCTL13	2085
#define IDC_MEMVALUECTL13	2086
#define IDC_MEMREADBTN13	2087
#define IDC_MEMWRITEBTN13	2088
#define IDC_MEMRDENBTN13	2089
#define IDC_MEMWRENBTN13	2090

#define IDC_MEMTYPECTL14	2091
#define IDC_MEMADDRCTL14	2092
#define IDC_MEMVALUECTL14	2093
#define IDC_MEMREADBTN14	2094
#define IDC_MEMWRITEBTN14	2095
#define IDC_MEMRDENBTN14	2096
#define IDC_MEMWRENBTN14	2097

#define IDC_MEMTYPECTL15	2098
#define IDC_MEMADDRCTL15	2099
#define IDC_MEMVALUECTL15	2100
#define IDC_MEMREADBTN15	2101
#define IDC_MEMWRITEBTN15	2102
#define IDC_MEMRDENBTN15	2103
#define IDC_MEMWRENBTN15	2104

#define IDC_MEMTYPECTL16	2105
#define IDC_MEMADDRCTL16	2106
#define IDC_MEMVALUECTL16	2107
#define IDC_MEMREADBTN16	2108
#define IDC_MEMWRITEBTN16	2109
#define IDC_MEMRDENBTN16	2110
#define IDC_MEMWRENBTN16	2111

#define IDC_MEMTYPECTL17	2112
#define IDC_MEMADDRCTL17	2113
#define IDC_MEMVALUECTL17	2114
#define IDC_MEMREADBTN17	2115
#define IDC_MEMWRITEBTN17	2116
#define IDC_MEMRDENBTN17	2117
#define IDC_MEMWRENBTN17	2118

#define IDC_MEMTYPECTL18	2119
#define IDC_MEMADDRCTL18	2120
#define IDC_MEMVALUECTL18	2121
#define IDC_MEMREADBTN18	2122
#define IDC_MEMWRITEBTN18	2123
#define IDC_MEMRDENBTN18	2124
#define IDC_MEMWRENBTN18	2125


typedef struct MEM_CTL_GRP_tag {
	CButton		WriteBtn;
	CButton		ReadBtn;
	CComboBox	Address;
	CComboBox	Type;
	CEdit		Data;
	CButton		WrEnBtn;
	CButton		RdEnBtn;
}MEM_CTL_GRP;

/////////////////////////////////////////////////////////////////////////////
// CMemAccessDlg dialog

class CMemAccessDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CMemAccessDlg)

// Construction
public:
	CMemAccessDlg();
	~CMemAccessDlg();

// Dialog Data
	//{{AFX_DATA(CMemAccessDlg)
	enum { IDD = IDD_MEMACESS_DIALOG };
	CString	m_Status;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CMemAccessDlg)
	public:
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void Write(int nID_COMBO_BOX, int memory_area_id, CString &datastring);
	void Read(int nID_COMBO_BOX, int memory_area_id, CString &resultstring);
	void LoadPIUAddressData(CString filename, UINT nID_COMBO_BOX);
	void LoadMemoryAreaData(UINT nID_COMBO_BOX);
	void SetUpMemAccessControls(int nFirstControlNum);
	void MakeControlLoopList(void);
	UINT GetMemTypeControlID(int nControlGroupNumber);
	UINT GetAddressControlID(int nControlGroupNumber);
	UINT GetDataValueControlID(int nControlGroupNumber);
	int nLoopCount;
	int nNextGroupNumberToLoop;
	int nDelay;
	bool bStopOnError;

	// Variables for dynamic Memory Read/Write controls
	MEM_CTL_GRP Controls[NUM_CONTROL_GROUPS];
	bool WriteControlsToLoop[NUM_CONTROL_GROUPS];
	bool ReadControlsToLoop[NUM_CONTROL_GROUPS];


	// Generated message map functions
	//{{AFX_MSG(CMemAccessDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtoncontread();
	afx_msg void OnButtoncontstop();
	afx_msg void OnCheckstoperror();
	afx_msg void OnDynamicRead();
	afx_msg void OnDynamicRead2();
	afx_msg void OnDynamicRead3();
	afx_msg void OnDynamicRead4();
	afx_msg void OnDynamicRead5();
	afx_msg void OnDynamicRead6();
	afx_msg void OnDynamicRead7();
	afx_msg void OnDynamicRead8();
	afx_msg void OnDynamicRead9();
	afx_msg void OnDynamicRead10();
	afx_msg void OnDynamicRead11();
	afx_msg void OnDynamicRead12();
	afx_msg void OnDynamicRead13();
	afx_msg void OnDynamicRead14();
	afx_msg void OnDynamicRead15();
	afx_msg void OnDynamicRead16();
	afx_msg void OnDynamicRead17();
	afx_msg void OnDynamicRead18();
	afx_msg void OnDynamicWrite();
	afx_msg void OnDynamicWrite2();
	afx_msg void OnDynamicWrite3();
	afx_msg void OnDynamicWrite4();
	afx_msg void OnDynamicWrite5();
	afx_msg void OnDynamicWrite6();
	afx_msg void OnDynamicWrite7();
	afx_msg void OnDynamicWrite8();
	afx_msg void OnDynamicWrite9();
	afx_msg void OnDynamicWrite10();
	afx_msg void OnDynamicWrite11();
	afx_msg void OnDynamicWrite12();
	afx_msg void OnDynamicWrite13();
	afx_msg void OnDynamicWrite14();
	afx_msg void OnDynamicWrite15();
	afx_msg void OnDynamicWrite16();
	afx_msg void OnDynamicWrite17();
	afx_msg void OnDynamicWrite18();
	afx_msg void OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MemAccessDlg_H__E3389355_BB89_45E5_95BF_32E201F48D59__INCLUDED_)
