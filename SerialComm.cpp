/*

Based on the original SEP peek/poke debugger application.

Version 3.1

  Baselined 8 April 2002

Version 3.4 Established 6 June, 2002

*/

// SerialComm.cpp: implementation of the CSerialComm class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SerialComm.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CSerialComm::CSerialComm() : m_commHandle(NULL), bReadZeroByteCountError(false),
    bWriteOddByteCountError(false), bReadOddByteCountError(false)
{
}

CSerialComm::~CSerialComm()
{
	if( m_commHandle != NULL )
			CloseHandle( m_commHandle );
}

/////////////////////////////////////////////////////////////////////////////
//	bool SendBytes(unsigned int count, bool esc_override /* = FALSE */)
/////////////////////////////////////////////////////////////////////////////
//	This function tries to write the specified number of bytes from 
//	cComString out the COM port. It escapes ENQ and ESC characters unless 
//	the esc_override flag is set to TRUE.
//	Note that the esc_override flag defaults to FALSE if the user does not
//	explicitly set it to true.
/////////////////////////////////////////////////////////////////////////////
int CSerialComm::SendBytes(int count)
{
	DWORD dwCount;
    bool bRetval = false;

    bWriteOddByteCountError = false;

    if (WriteFile(m_commHandle, cComBuff, count * 2, &dwCount, NULL)) {
        bRetval = (dwCount == ((DWORD)count * 2));
        
        if( !bRetval ) {
            bWriteOddByteCountError = true;
        }
    }

    return bRetval;
}


/////////////////////////////////////////////////////////////////////////////
//	bool GetData(unsigned int count)
/////////////////////////////////////////////////////////////////////////////
bool CSerialComm::GetData( int count )
{
    DWORD dwCount;
    bool bRetval = false;

    bReadOddByteCountError = false;
    bReadZeroByteCountError = false;

    if (ReadFile( m_commHandle, cComBuff, count * 2, &dwCount, NULL)) {
        if (dwCount == 0) {
            bReadZeroByteCountError = true;
        } else {
            bRetval = (dwCount == ((DWORD)count * 2));

            if (!bRetval) {
                bReadOddByteCountError = true;
            }
        }
    }

	return bRetval;
}

/////////////////////////////////////////////////////////////////////////////
//	bool OpenPort()
/////////////////////////////////////////////////////////////////////////////
//	This function sets the port handle variable m_commHandle - which is
//	used by other CSerialComm functions to read from and write to the COM
//	port. This function closes the current handle (if there is one), then
//	attempts to create a new handle for the COM port selected in the 
//	control dialog. Finally, it sets the properties to work with the hardware
//	(baud rate, byte size, parity, stop bits, timeouts).
/////////////////////////////////////////////////////////////////////////////
bool CSerialComm::CommPortPresent( LPCTSTR portName ) {

	HANDLE m_testHandle;

	// try to create a handle for the port.
	m_testHandle = CreateFile( portName,
				               GENERIC_READ | GENERIC_WRITE,
				               0,
				               NULL,
				               OPEN_EXISTING,
				               FILE_ATTRIBUTE_NORMAL,
				               NULL );

	if (m_testHandle != INVALID_HANDLE_VALUE)
	{
		CloseHandle(m_testHandle);
	}

	// If we found a valid port then we'll return a true,
	return (m_testHandle != INVALID_HANDLE_VALUE);
}

void CSerialComm::ClosePort(void) {
	// If we already have a port handle then release it.
	if (m_commHandle != NULL)
	{
		CloseHandle(m_commHandle);
		m_commHandle = NULL;
	}

};

bool CSerialComm::OpenPort( LPCTSTR portName )
{
	DWORD dwSize;
	DCB dcb;
	COMMTIMEOUTS timeouts;	
	bool bResult;

	// If we already have a port handle then release it.
	if( m_commHandle != NULL )
	{
		CloseHandle( m_commHandle );
	}

	// Create a handle for the port.
	m_commHandle = CreateFile( portName,
				               GENERIC_READ | GENERIC_WRITE,
				               0,
				               NULL,
				               OPEN_EXISTING,
				               FILE_ATTRIBUTE_NORMAL,
				               NULL );
	
	if(m_commHandle == INVALID_HANDLE_VALUE) {
		bResult = FALSE;
	}
	else 
	{
		bResult = TRUE;
		// Set the port's properties.
		LPCOMMPROP properties = (LPCOMMPROP)malloc (sizeof(COMMPROP));
		properties->wPacketLength = sizeof(COMMPROP);
		GetCommProperties( m_commHandle, properties);
		dwSize = properties->wPacketLength;
		free(properties);
		properties = (LPCOMMPROP)malloc (dwSize);
		properties->wPacketLength = (WORD)dwSize;
		GetCommProperties(m_commHandle, properties);
		GetCommState(m_commHandle, &dcb);
        
		if (properties->dwSettableParams & SP_HANDSHAKING)
        {
			// dcb.fOutxDsrFlow = TRUE;
			dcb.fOutxCtsFlow = FALSE;
			dcb.fOutxDsrFlow = FALSE;
			dcb.fDtrControl = DTR_CONTROL_DISABLE;
			dcb.fOutX = FALSE;
			dcb.fInX = FALSE;
			dcb.fRtsControl = RTS_CONTROL_DISABLE;
		}

		dcb.BaudRate = 38400;

		if(properties->wSettableData & DATABITS_8){	// The rest of this is constant
			dcb.ByteSize = DATABITS_8;				// and should probably be allowed on any PC
        }											// in the world, but we'll check just in case

		if(properties->wSettableStopParity)
		{
			dcb.fParity = 1;
			dcb.Parity = NOPARITY;
			dcb.StopBits = ONESTOPBIT;
		}

		SetCommState(m_commHandle, &dcb);

		GetCommTimeouts(m_commHandle, &timeouts);
		timeouts.ReadTotalTimeoutMultiplier = 1000;
		timeouts.WriteTotalTimeoutMultiplier = 1000;
		SetCommTimeouts(m_commHandle, &timeouts);
		free(properties);
	}

	return bResult;
}

void CSerialComm::PurgeRxBuffer()
{

	PurgeComm(m_commHandle, PURGE_RXABORT | PURGE_RXCLEAR);
}

void CSerialComm::GetErrorMsgString(CString &str)
{
    LPVOID lpMsgBuf;

    if( bWriteOddByteCountError ) {
        str = "Odd number of bytes written to com port.";
    } else if( bReadOddByteCountError ) {
        str = "Odd number of bytes read from com port.";
    } else if( bReadZeroByteCountError ) {
        str = "Zero number of bytes read from com port.";
    } else {
        FormatMessage( 
            FORMAT_MESSAGE_ALLOCATE_BUFFER | 
            FORMAT_MESSAGE_FROM_SYSTEM | 
            FORMAT_MESSAGE_IGNORE_INSERTS,
            NULL,
            GetLastError(),
            0, // Default language
            (LPTSTR) &lpMsgBuf,
            0,
            NULL 
        );

        str = (LPCTSTR)lpMsgBuf;
    
        // Free the buffer.
        LocalFree( lpMsgBuf );
    }

    // Reset error message flags
    bWriteOddByteCountError = false;
    bReadOddByteCountError = false;
    bReadZeroByteCountError = false;
}
