// Globals.cpp
// Global Classes and variables for all property pages to use
//

#include "stdafx.h"
#include "Commands.h"
#include "SerialComm.h"
#include <fstream>

CCommands commandEngine;
CSerialComm serialObj;
CString szAddrFileName = "addresses.txt";

bool bEnableSafetyCriticalWrites;
bool bDataLoggingEnabled;
bool bErrorDetected;
bool bAdvancedBootloaderAvailable;
bool bComEstablished;
std::ofstream logfile;
int NumberOfAvailableInputFiles = 0;