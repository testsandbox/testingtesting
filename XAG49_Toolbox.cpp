// XAG49_Toolbox.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "XAG49_Toolbox.h"
//#include "XAG49_ToolboxDlg.h"
#include "ConnectDlg.h"
#include "MemAccessDlg.h"
#include "ScriptDlg.h"
#include "MemDumpDlg.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CXAG49_ToolboxApp

BEGIN_MESSAGE_MAP(CXAG49_ToolboxApp, CWinApp)
	//{{AFX_MSG_MAP(CXAG49_ToolboxApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CXAG49_ToolboxApp construction

CXAG49_ToolboxApp::CXAG49_ToolboxApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CXAG49_ToolboxApp object

CXAG49_ToolboxApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CXAG49_ToolboxApp initialization

BOOL CXAG49_ToolboxApp::InitInstance()
{
	AfxEnableControlContainer();

	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	CPropertySheet	myPropSheet;
	CConnectDlg		Page1;
	CMemAccessDlg	Page2;
	CMemDumpDlg		Page3;
	CScriptDlg		Page4;

	myPropSheet.AddPage(&Page1);
	myPropSheet.AddPage(&Page2);
	myPropSheet.AddPage(&Page3);
	myPropSheet.AddPage(&Page4);
	myPropSheet.SetTitle("XAG49_Toolbox (Ver: Jan 3 2007) NSWC Crane PM10");

	int nResponse = myPropSheet.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Close log file if open and app is terminating
	if( logfile.is_open() ) {
		logfile.close();
	}


	/*
	CXAG49_ToolboxDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}
	*/

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}
