// MemDumpDlg.cpp : implementation file
//

#include "stdafx.h"
#include "XAG49_Toolbox.h"
#include "MemDumpDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMemDumpDlg property page

IMPLEMENT_DYNCREATE(CMemDumpDlg, CPropertyPage)


CMemDumpDlg::CMemDumpDlg() : CPropertyPage(CMemDumpDlg::IDD)
{
	//{{AFX_DATA_INIT(CMemDumpDlg)
	m_Status = _T("");
	m_szStartAddress = _T("");
	m_szEndAddress = _T("");
	m_szMemDumpData = _T("");
	//}}AFX_DATA_INIT
}

CMemDumpDlg::~CMemDumpDlg()
{
}

void CMemDumpDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMemDumpDlg)
	DDX_Text(pDX, IDC_EDITSTATUSDUMP, m_Status);
	DDX_Text(pDX, IDC_EDITSTARTADDR, m_szStartAddress);
	DDX_Text(pDX, IDC_EDITENDADDR, m_szEndAddress);
	DDX_Text(pDX, IDC_EDITMEMDUMP, m_szMemDumpData);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMemDumpDlg, CDialog)
	//{{AFX_MSG_MAP(CMemDumpDlg)
	ON_BN_CLICKED(IDC_BUTTONCLEARDISPLAY, OnButtoncleardisplay)
	ON_BN_CLICKED(IDC_BUTTONREAD, OnButtonread)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMemDumpDlg message handlers

void CMemDumpDlg::OnButtoncleardisplay() 
{
	m_szMemDumpData.Empty();
	
	// update the dialog
    UpdateData(FALSE);
}

void CMemDumpDlg::OnButtonread() 
{
	WORD AddressContents;
	bool bErrorDetected = false;

    // grab the current address from the dialog
    UpdateData(TRUE);

    unsigned int StartAddress2Read = strtol(m_szStartAddress, NULL, 16);
	unsigned int EndAddress2Read = strtol(m_szEndAddress, NULL, 16);

	CComboBox *pcb = (CComboBox *)GetDlgItem(IDC_COMBOMEMTYPE);
	
	if(bAdvancedBootloaderAvailable) {
		MemoryArea ma = DisableAll;

		switch (pcb->GetCurSel()) {
		default:
			ma = DisableAll;
			break;
		case MAI_SFR:
			ma = CPU_SFR;
			break;
		case MAI_CPU_RAM:
			ma = CPU_RAM;
			break;
		case MAI_CPU_ROM:
			ma = CPU_Code;
			break;
		}

		if (!commandEngine.SelectMemory(ma)) {
        	commandEngine.GetErrorMsgString(m_Status);
        	bErrorDetected = true;
		}
	}
	
	if(!bErrorDetected) {
		// read data from addresses
		for(unsigned int Addr = StartAddress2Read; Addr <= EndAddress2Read && !bErrorDetected; Addr+=2)
		{
    		if(commandEngine.Peek( Addr, &AddressContents )) {

        		m_Status = "";
				m_Status.Format("Address 0x%X = 0x%X\r\n",Addr, AddressContents);
    		} else {
        		commandEngine.GetErrorMsgString(m_Status);
        		bErrorDetected = true;
    		}

    		if(bDataLoggingEnabled) {
        		logfile << (LPCTSTR)m_Status << std::endl;
			}

			m_szMemDumpData += m_Status;
		}
    }

    // update the dialog
    UpdateData(FALSE);
	
}

BOOL CMemDumpDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	CComboBox * pCombo;

	pCombo = (CComboBox *)GetDlgItem(IDC_COMBOMEMTYPE);
	pCombo->SetCurSel(0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
