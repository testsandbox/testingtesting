#if !defined(AFX_SCRIPTDLG_H__2EF5E67C_B223_4DC2_A475_4984FD7829D1__INCLUDED_)
#define AFX_SCRIPTDLG_H__2EF5E67C_B223_4DC2_A475_4984FD7829D1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ScriptDlg.h : header file
//

enum eActionTypes_t
{
    READ,
    WRITE,
    PAUSE
};

struct StepData_t {
    eActionTypes_t  eActionType;
    unsigned int    uAddress;
	unsigned int	uData;
};

struct StackData_t {
	WORD	uCommand;
	WORD	uParam1;
	WORD	uParam2;
	WORD	uParam3;
	WORD	uParam4;
	WORD	uParam5;
	WORD	uParam6;
	WORD	uParam7;
	WORD	uParam8;
};

#define PEEKPOKE_SCRIPT_EXEC_TIMER		10
#define BL_SCRIPT_EXECUTION_TIMER		20

#define NUM_OF_STACK_ENTRIES 100
#define NUM_OF_SCRIPT_ENTRIES 100
#define SCRIPT_BUFFER_SIZE    1024

#define PUSH_CMD						1
#define DUP_CMD							2
#define SWAP_CMD						3
#define POP_CMD							4
#define XMIT_CMD						5
#define READ_XDATA_CMD					6
#define WRITE_XDATA_CMD					7
#define READ_IDATA_CMD					8
#define WRITE_IDATA_CMD					9
#define READ_SFR_CMD					10
#define WRITE_SFR_CMD					11
#define EQUAL_CMD						12
#define NOT_EQUAL_CMD					13
#define GREATER_THAN_OR_EQUAL_CMD		14
#define GREATER_THAN_CMD				15
#define LESS_THAN_OR_EQUAL_CMD			16
#define LESS_THAN_CMD					17
#define LOGICAL_AND_CMD					18
#define LOGICAL_OR_CMD					19
#define LOGICAL_COMPLEMENT_CMD			20
#define BITWISE_AND_CMD					21
#define BITWISE_OR_CMD					22
#define BITWISE_COMPLEMENT_CMD			23
#define SHIFT_LEFT_CMD					24
#define SHIFT_RIGHT_CMD					25
#define ADD_CMD							26
#define SUBTRACT_CMD					27
#define REPEAT_CMD						28
#define END_REPEAT_CMD					29
#define WHILE_CMD						30
#define END_WHILE_CMD					31
#define IF_CMD							32
#define END_IF_CMD						33
#define DELAY_CMD						34
#define WR_RD_XDATA_CMD					35
#define POP_TO_XDATA_CMD				36
#define WR_WR_XDATA_CMD					37
#define NOP_CMD							38
#define RESET_DATA_STACK				39
#define PUSH_MAX_VALUE_OF_DATA_STACK	40
#define PUSH_MIN_VALUE_OF_DATA_STACK	41
#define PUSH_SUM_OF_DATA_STACK			42
#define SIGN_EXTEND_DATA_STACK			43
#define ABSOLUTE_VALUE_DATA_STACK		44
#define QUIT_CMD						45
#define READ_XDATA2_CMD					46
#define WRITE_XDATA2_CMD				47
#define READ_IDATA2_CMD					48
#define WRITE_IDATA2_CMD				49
#define READ_SFR2_CMD					50
#define WRITE_SFR2_CMD					51


/////////////////////////////////////////////////////////////////////////////
// CScriptDlg dialog

class CScriptDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CScriptDlg)

// Construction
public:
	CScriptDlg();
	~CScriptDlg();

// Dialog Data
	//{{AFX_DATA(CScriptDlg)
	enum { IDD = IDD_SCRIPT_DIALOG };
	CEdit	m_EditParam7Hint;
	CEdit	m_EditParam8Hint;
	CEdit	m_EditParam8;
	CEdit	m_EditParam7;
	CEdit	m_EditParam6Hint;
	CEdit	m_EditParam6;
	CEdit	m_EditParam5Hint;
	CEdit	m_EditParam5;
	CEdit	m_EditParam4Hint;
	CEdit	m_EditParam4;
	CSpinButtonCtrl	m_SpinStackIndex;
	CEdit	m_EditParam3Hint;
	CEdit	m_EditParam2Hint;
	CEdit	m_EditParam1Hint;
	CEdit	m_EditParam3;
	CEdit	m_EditParam2;
	CEdit	m_EditParam1;
	CComboBox	m_ComboCommand;
	CString	m_Status;
	CString	m_StackDisplay;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CScriptDlg)
	public:
	virtual BOOL OnSetActive();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void RegenerateStackDisplay(int nCurrentStackIndex, bool bIsForFile = false);
	int StackToCommBuffer();
	void ControlDataToStack(int nIndex);
	void SetUpScriptDataControls(int nCmdIndex, bool bCopyStackData, int nStackDataIndex);
	int nScriptIndex;
	int nStackIndex;
	int nCurrentScriptIndex;
	StepData_t StepData[NUM_OF_SCRIPT_ENTRIES];
	StackData_t StackData[NUM_OF_STACK_ENTRIES];
	WORD ComData[SCRIPT_BUFFER_SIZE];
	// Generated message map functions
	//{{AFX_MSG(CScriptDlg)
	afx_msg void OnButtonrunscript();
	afx_msg void OnButtoneditscriptfile();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnButtonaddcommand();
	afx_msg void OnButtonremovecommand();
	afx_msg void OnButtonrunblscript();
	afx_msg void OnButtonclearscript();
	afx_msg void OnSelchangeComboactions();
	afx_msg void OnDeltaposSpinscriptindex(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnButtoninsertcommand();
	afx_msg void OnButtonupdatecommand();
	afx_msg void OnButtonsendscript();
	afx_msg void OnButtonstopdatamonitor();
	afx_msg void OnButtonsavescript();
	afx_msg void OnButtonloadscript();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SCRIPTDLG_H__2EF5E67C_B223_4DC2_A475_4984FD7829D1__INCLUDED_)
