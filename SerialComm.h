// SerialComm.h: interface for the CSerialComm class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SERIALCOMM_H__076F64D5_963C_11D1_8F35_00A024C9D056__INCLUDED_)
#define AFX_SERIALCOMM_H__076F64D5_963C_11D1_8F35_00A024C9D056__INCLUDED_

#include "stdafx.h"

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

class CSerialComm  
{
public:
	bool OpenPort(LPCTSTR portName);
	void ClosePort(void);
	bool CommPortPresent(LPCTSTR portName);
	int SendBytes(int count);
	bool GetData(int count);
	void PurgeRxBuffer(void);
	void SendReset();
    void GetErrorMsgString(CString &str);
	CSerialComm();
	~CSerialComm();
	unsigned short cComBuff[260];
private:
	HANDLE m_commHandle;
    bool bReadZeroByteCountError;
	bool bWriteOddByteCountError;
    bool bReadOddByteCountError;
};

#endif // !defined(AFX_SERIALCOMM_H__076F64D5_963C_11D1_8F35_00A024C9D056__INCLUDED_)
