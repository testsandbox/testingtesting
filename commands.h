// Commands.h: interface for the CCommands class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_COMMANDS_H__36B57301_A6A9_11D1_8F35_00A024C9D056__INCLUDED_)
#define AFX_COMMANDS_H__36B57301_A6A9_11D1_8F35_00A024C9D056__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "SerialComm.h"

typedef enum MemoryArea {
    DisableAll = 0, UCCard = 0x01, AccessoryA = 0x02, AccessoryB = 0x03,
    AccessoryC = 0x04, AccessoryD = 0x05, OFPMemory = 0x06, FactoryData = 0x07,
    CPU_SFR = 0x08, CPU_RAM = 0x09, CPU_Code = 0x0A, ExternalData =  0x0B};

class CCommands  
{
public:
	bool Ping( void );
	bool ConnectPing(void);
	bool SendData(int SendCount, unsigned short ResponseInterval, unsigned short * data);
	bool SendCommand(int ReceiveCount, WORD CommandInterval, WORD * data);
	bool Poke( unsigned int DestinationAddress, WORD data, bool SafetyCriticalSignal = true );
	bool Peek( unsigned int SourceAddress, WORD * data  );
	void ResetComms();	// Reset communications to idle state

	unsigned int PIUMemConfig(unsigned short * CBuffer);

	void SetSerialObject(CSerialComm * serial);
	void GetErrorMsgString(CString &str);
    WORD GetLastStatus(void);
	
	bool SelectMemory(MemoryArea Area);
	bool ScriptLoad( unsigned int DestinationAddress, WORD *data, BYTE numWords );
	bool ExecuteBLScript(bool &bGotData, WORD *data);

	CSerialComm * p_serial;
	CCommands();
	~CCommands();
private:
	bool bIsComError;
    bool bIsCmdError;
    bool bIsChecksumError;
	
    WORD StatusWord;
    WORD ExpectedStatusWord;
};

#endif // !defined(AFX_COMMANDS_H__36B57301_A6A9_11D1_8F35_00A024C9D056__INCLUDED_)
