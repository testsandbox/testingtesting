#if !defined(AFX_MEMDUMPDLG_H__D1DC3BFD_CEE2_4A8E_B3ED_6A1B2E30F2AE__INCLUDED_)
#define AFX_MEMDUMPDLG_H__D1DC3BFD_CEE2_4A8E_B3ED_6A1B2E30F2AE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MemDumpDlg.h : header file
//

#include "Globals.h"

#define MAI_NONE	(0)
#define MAI_SFR		(1)
#define MAI_CPU_RAM (2)
#define MAI_CPU_ROM (3)

/////////////////////////////////////////////////////////////////////////////
// CMemDumpDlg dialog

class CMemDumpDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CMemDumpDlg)

// Construction
public:
	CMemDumpDlg();
	~CMemDumpDlg();

// Dialog Data
	//{{AFX_DATA(CMemDumpDlg)
	enum { IDD = IDD_MEMDUMP_DIALOG };
	CString	m_Status;
	CString	m_szStartAddress;
	CString	m_szEndAddress;
	CString	m_szMemDumpData;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMemDumpDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMemDumpDlg)
	afx_msg void OnButtoncleardisplay();
	afx_msg void OnButtonread();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MEMDUMPDLG_H__D1DC3BFD_CEE2_4A8E_B3ED_6A1B2E30F2AE__INCLUDED_)
