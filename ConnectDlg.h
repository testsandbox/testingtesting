#if !defined(AFX_CONNECTDLG_H__6AAFD9D9_AB42_492C_8BA7_BCFFE9C1F1AB__INCLUDED_)
#define AFX_CONNECTDLG_H__6AAFD9D9_AB42_492C_8BA7_BCFFE9C1F1AB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "Commands.h"
#include "SerialComm.h"
#include <fstream>

#define NUM_PIUINFO_COLUMNS 7

// ConnectDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CConnectDlg dialog

class CConnectDlg : public CPropertyPage
{
	DECLARE_DYNCREATE(CConnectDlg)

// Construction
public:
	CConnectDlg();
	~CConnectDlg();

// Dialog Data
	//{{AFX_DATA(CConnectDlg)
	enum { IDD = IDD_CONNECT_DIALOG };
	CString	m_Status;
	//}}AFX_DATA


// Overrides
	// ClassWizard generate virtual function overrides
	//{{AFX_VIRTUAL(CConnectDlg)
	public:
	virtual void OnCancel();
	virtual void OnOK();
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void ActivateComDependentControls(bool bEnable);
	CListCtrl PIUInfoList;
	void SetUpPIUInfoControl();
	CString VerNum(WORD v);
	bool GetPIUPartNumber(CString &tStr);
	bool GetPIUSerialNumber(CString &tStr);
	bool GetPIU_SWVer(CString &tStr);
	// Generated message map functions
	//{{AFX_MSG(CConnectDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonclr();
	afx_msg void OnConnect();
	afx_msg void OnButtongetinfo();
	afx_msg void OnChecksafety();
	afx_msg void OnChecklogenable();
	afx_msg void OnCheckadvbootloader();
	afx_msg void OnButtonedittextfile();
	afx_msg void OnButtonloadaddress();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONNECTDLG_H__6AAFD9D9_AB42_492C_8BA7_BCFFE9C1F1AB__INCLUDED_)
