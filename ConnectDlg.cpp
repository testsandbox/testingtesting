// ConnectDlg.cpp : implementation file
//

#include "stdafx.h"
#include "XAG49_Toolbox.h"
#include "ConnectDlg.h"
#include "Globals.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

static _TCHAR *_gszPIUInfoLabel[NUM_PIUINFO_COLUMNS] =
{
	_T("Serial #"),  _T("Part #"), _T("Bootload"), _T("PIU SW"),   _T("uC"),  _T("ACC A/C"), _T("ACC B/D")
};

CString pNames[] = {"COM1","COM2","COM3","COM4","COM5","COM6","COM7","COM8","COM9"};

/////////////////////////////////////////////////////////////////////////////
// CConnectDlg property page

IMPLEMENT_DYNCREATE(CConnectDlg, CPropertyPage)

CConnectDlg::CConnectDlg() : CPropertyPage(CConnectDlg::IDD)
{
	//{{AFX_DATA_INIT(CConnectDlg)
	m_Status = _T("");
	//}}AFX_DATA_INIT
}

CConnectDlg::~CConnectDlg()
{
}

void CConnectDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConnectDlg)
	DDX_Text(pDX, IDC_EDITSTATUS, m_Status);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConnectDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CConnectDlg)
	ON_BN_CLICKED(IDC_BUTTONCLR, OnButtonclr)
	ON_BN_CLICKED(IDCONNECT, OnConnect)
	ON_BN_CLICKED(IDC_BUTTONGETINFO, OnButtongetinfo)
	ON_BN_CLICKED(IDC_CHECKSAFETY, OnChecksafety)
	ON_BN_CLICKED(IDC_CHECKLOGENABLE, OnChecklogenable)
	ON_BN_CLICKED(IDC_CHECKADVBOOTLOADER, OnCheckadvbootloader)
	ON_BN_CLICKED(IDC_BUTTONEDITTEXTFILE, OnButtonedittextfile)
	ON_BN_CLICKED(IDC_BUTTONLOADADDRESS, OnButtonloadaddress)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConnectDlg message handlers

BOOL CConnectDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();

	// Disable com dependent buttons until connection is made
	ActivateComDependentControls(false);
	bComEstablished = false;
	
	// Default to make all writes safety critical
	CButton *pBtn = (CButton *)GetDlgItem(IDC_CHECKSAFETY);
	pBtn->SetCheck(TRUE);
	bEnableSafetyCriticalWrites = true;

	// Data logging is turned off by default
	bDataLoggingEnabled = false;

	// Advanced Bootloader not loaded by default
	bAdvancedBootloaderAvailable = false;

	// Setup PIU Info Area
	SetUpPIUInfoControl();

	// Initialize serial object pointer for command class
	commandEngine.SetSerialObject(&serialObj);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CConnectDlg::OnButtonclr() 
{
	for(int j = 0; j<NUM_PIUINFO_COLUMNS; j++)
	{
		PIUInfoList.SetItemText(0,j,_T("Cleared"));
	}
	
    UpdateData(FALSE);	
}

void CConnectDlg::OnConnect() 
{
	CString commport;

	// Disable exit button until we are done trying to establish com with PIU
	//CButton * pBtn = (CButton *)GetDlgItem(IDCANCEL);
	//pBtn->EnableWindow(FALSE);

    for (int j = 0; j < 3; j++)
    {
        for (int i = 0; i < 9; i++)
        {
            if(serialObj.CommPortPresent(pNames[i]))
            {
                if(serialObj.OpenPort(pNames[i]))
                {
                    if (commandEngine.ConnectPing()) 
                    {
				        m_Status = "Connected to PIU";

                        // Enable com dependent buttons
				        ActivateComDependentControls(true);

                        bComEstablished = true;

                        SetDlgItemText(IDC_EDITCOMPORT,pNames[i]);

                        i = j = 10;
                    }
                }
            }
        }
    }

    if(!bComEstablished) {
		m_Status = "Could not find a PIU on any com port.";
    }

	UpdateData(FALSE);	
}

void CConnectDlg::OnButtongetinfo() 
{
	CString szStr;
	
	WORD Buffer[ 4 ];

    Buffer[ 0 ] = 0xF104;

    // Get Bootloader, uC FPGA, Int C FPGA, and Int D FPGA version numbers
    if (commandEngine.SendCommand( 4, 25, Buffer )) {
		PIUInfoList.SetItemText(0,2,VerNum(Buffer[0]));
		PIUInfoList.SetItemText(0,4,VerNum(Buffer[1]));
		PIUInfoList.SetItemText(0,5,VerNum(Buffer[2]));
		PIUInfoList.SetItemText(0,6,VerNum(Buffer[3]));

        // Get PIU part number
        if(GetPIUPartNumber(szStr)) {
			PIUInfoList.SetItemText(0,1,szStr);

            // Get PIU serial number
            if(GetPIUSerialNumber(szStr)) {
				PIUInfoList.SetItemText(0,0,szStr);
                
                // Get PIU sw version
                if(!GetPIU_SWVer(szStr)) {
                    commandEngine.GetErrorMsgString(m_Status);
                } else {
					PIUInfoList.SetItemText(0,3,szStr);
				}
            } else {
                commandEngine.GetErrorMsgString(m_Status);
            }
        } else {
                commandEngine.GetErrorMsgString(m_Status);
        }
    } else {
        commandEngine.GetErrorMsgString(m_Status);
    }

    UpdateData(FALSE);
}


void CConnectDlg::OnChecksafety() 
{
	CButton *pBtn = (CButton *)GetDlgItem(IDC_CHECKSAFETY);
	if (pBtn->GetCheck()) {
		bEnableSafetyCriticalWrites = true;
	} else {
		bEnableSafetyCriticalWrites = false;
	}	
}

void CConnectDlg::OnChecklogenable() 
{
	CButton *pBtn = (CButton *)GetDlgItem(IDC_CHECKLOGENABLE);

	if(pBtn->GetCheck()) {

		// Close log file if open
		if( logfile.is_open() ) {
			logfile.close();
		}
		bDataLoggingEnabled = FALSE;

		CString filename;
		// Launch the file open dialog to get file name

		static char BASED_CODE szFilter[] = "Log Files (*.txt)|*.txt|";
		CFileDialog dlgFile(FALSE,NULL,NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter);
		if(dlgFile.DoModal() == IDOK)
		{
			filename = dlgFile.GetPathName();
			
			logfile.open(filename);

			if( logfile.is_open() ) {
				bDataLoggingEnabled = TRUE;
				pBtn->SetWindowText("Data Logging Enabled");
			}
		} else {
			pBtn->SetCheck(FALSE);
		}
	} else {
		if( logfile.is_open() ) {
			logfile.close();
		}
		pBtn->SetWindowText("Enable Data Logging");
		bDataLoggingEnabled = FALSE;
	}	
}

void CConnectDlg::OnCheckadvbootloader() 
{
	CButton *pBtn = (CButton *)GetDlgItem(IDC_CHECKADVBOOTLOADER);
	if (pBtn->GetCheck()) {
		bAdvancedBootloaderAvailable = true;
	} else {
		bAdvancedBootloaderAvailable = false;
	}	
}

///////////////////////////////////////////////////////////////////////////////
// Helper Functions
///////////////////////////////////////////////////////////////////////////////
void CConnectDlg::ActivateComDependentControls(bool bEnable)
{
	CButton * pBtn = (CButton *)GetDlgItem(IDC_BUTTONGETINFO);
	pBtn->EnableWindow(bEnable);
}

void CConnectDlg::SetUpPIUInfoControl()
{
	// Determine window area for the control
	RECT InfoAreaPos;

	CWnd* pWnd = GetDlgItem(IDC_PIUINFO);
	pWnd->GetWindowRect(&InfoAreaPos);

	ScreenToClient(&InfoAreaPos);
	
	// Create control
	PIUInfoList.Create(WS_CHILD|WS_VISIBLE|WS_BORDER|LVS_REPORT,InfoAreaPos,this,1); 

	// insert columns

	int i, j;
	LV_COLUMN lvc;

	lvc.mask = LVCF_FMT | LVCF_WIDTH | LVCF_TEXT | LVCF_SUBITEM;

	for(i = 0; i<NUM_PIUINFO_COLUMNS; i++)
	{
		lvc.iSubItem = i;
		lvc.pszText = _gszPIUInfoLabel[i];
		lvc.cx = 100;
		lvc.fmt = LVCFMT_LEFT;
		PIUInfoList.InsertColumn(i,&lvc);
	}

	// insert single item

	LV_ITEM lvi;
	
	lvi.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_STATE;
	lvi.iItem = 0;
	lvi.iSubItem = 0;
	lvi.pszText = _T("No Info Yet");
	lvi.iImage = 0;
	lvi.stateMask = LVIS_STATEIMAGEMASK;
	lvi.state = INDEXTOSTATEIMAGEMASK(1);

	PIUInfoList.InsertItem(&lvi);

	// set item text for additional columns

	for(j = 1; j<NUM_PIUINFO_COLUMNS; j++)
	{
		PIUInfoList.SetItemText(0,j,_T("No Info Yet"));
	}
}

CString CConnectDlg::VerNum( WORD v)
{
	CString tstr;

	tstr.Format( "%d%d.%d%d", 
        (v >> 12) & 0xF,
        (v >> 8) & 0xF,
        (v >> 4) & 0xF,
        v & 0xF );

	return tstr;
}

bool CConnectDlg::GetPIUPartNumber( CString &tStr )
{	
    bool bRetval = false;
	int SerAddr = 0x78000;   // Start of POP_FLASH_Extrn_NVM_Factory_Data;
	WORD T1, T2, T3, T4, T5, T6, T7, T8;

    tStr = "unknown";	
	SerAddr += 0x1A;         // Serial Number Info starts at Data[ 13 ];
	
    if(commandEngine.Peek( SerAddr, &T1 )) {
	    SerAddr += 2;
        if(commandEngine.Peek( SerAddr, &T2 )) {
	        SerAddr += 2;
            if(commandEngine.Peek( SerAddr, &T3 )) {
	            SerAddr += 2;
                if(commandEngine.Peek( SerAddr, &T4 )) {
	                SerAddr += 2;
                    if(commandEngine.Peek( SerAddr, &T5 )) {
	                    SerAddr += 2;
                        if(commandEngine.Peek( SerAddr, &T6 )) {
	                        SerAddr += 2;
                            if(commandEngine.Peek( SerAddr, &T7 )) {
	                            SerAddr += 2;
                                if(commandEngine.Peek( SerAddr, &T8 )) {
                                    tStr = static_cast<char>(( T1 >> 8 ) & 0xFF);
		                            tStr += ( char ) (T1 & 0xFF);
		                            tStr += ( char ) (( T2 >> 8 ) & 0xFF);
		                            tStr += ( char ) (T2 & 0xFF);
		                            tStr += ( char ) (( T3 >> 8 ) & 0xFF);
		                            tStr += ( char ) (T3 & 0xFF);
		                            tStr += ( char ) (( T4 >> 8 ) & 0xFF);
		                            tStr += ( char ) (T4 & 0xFF);
		                            tStr += ( char ) (( T5 >> 8 ) & 0xFF);
		                            tStr += ( char ) (T5 & 0xFF);
		                            tStr += ( char ) (( T6 >> 8 ) & 0xFF);
		                            tStr += ( char ) (T6 & 0xFF);
		                            tStr += ( char ) (( T7 >> 8 ) & 0xFF);
		                            tStr += ( char ) (T7 & 0xFF);
		                            tStr += ( char ) (( T8 >> 8 ) & 0xFF);
		                            tStr += ( char ) (T8 & 0xFF);

                                    bRetval = true;
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    if(!bRetval) {
        commandEngine.GetErrorMsgString(m_Status);
        if(bDataLoggingEnabled) {
	        logfile << (LPCTSTR)m_Status << std::endl;
        }
    }
	
	return bRetval;
}

bool CConnectDlg::GetPIUSerialNumber( CString &tStr )
{
    bool bRetval = false;
	int SerAddr = 0x78000; // Start of POP_FLASH_Extrn_NVM_Factory_Data;
    WORD T1, T2, T3;
	
    tStr = "unknown";
	SerAddr += 0x2A;         // Serial Number Info starts at Data[ 21 ];
	
    if(commandEngine.Peek( SerAddr, &T1 )) {
	    SerAddr += 2;
        if(commandEngine.Peek( SerAddr, &T2 )){
	        SerAddr += 2;
            if(commandEngine.Peek( SerAddr, &T3 )){
                tStr = ( char ) (( T1 >> 8 ) & 0xFF);
		        tStr += ( char ) (T1 & 0xFF);
		        tStr += ( char ) (( T2 >> 8 ) & 0xFF);
		        tStr += ( char ) (T2 & 0xFF);
		        tStr += ( char ) (( T3 >> 8 ) & 0xFF);
		        tStr += ( char ) (T3 & 0xFF);

                bRetval = true;
            }
        }
    }

    if(!bRetval) {
        commandEngine.GetErrorMsgString(m_Status);
        if(bDataLoggingEnabled) {
	        logfile << (LPCTSTR)m_Status << std::endl;
        }
    }
	
	return bRetval;
}

bool CConnectDlg::GetPIU_SWVer( CString &tStr )
{
    bool bRetval = false;
	int SerAddr = 0x78000; // Start of POP_FLASH_Extrn_NVM_Factory_Data;
	WORD T1, T2, T3;
	
   	tStr = "unknown";
	SerAddr += 0x10;       // Serial Number Info starts at Data[ 8 ] ( Word 8, byte 16 );
	
	if(commandEngine.Peek( SerAddr, &T1 )) {
	    SerAddr += 2;
	    if(commandEngine.Peek( SerAddr, &T2 )) {
	        SerAddr += 2;
	        if(commandEngine.Peek( SerAddr, &T3 )) {
		        tStr = ( char ) (( T1 >> 8 ) & 0xFF);
		        tStr += ( char ) (T1 & 0xFF);
		        tStr += ( char ) (( T2 >> 8 ) & 0xFF);
		        tStr += ( char ) (T2 & 0xFF);
		        tStr += ( char ) (( T3 >> 8 ) & 0xFF);
		        tStr += ( char ) (T3 & 0xFF);

                bRetval = true;
            }
        }
    }

    if(!bRetval) {
        commandEngine.GetErrorMsgString(m_Status);
        if(bDataLoggingEnabled) {
    	  logfile << (LPCTSTR)m_Status << std::endl;
        }
    }
	
	return bRetval;
}

void CConnectDlg::OnCancel() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CPropertyPage::OnCancel();
}

void CConnectDlg::OnOK() 
{
	// TODO: Add your specialized code here and/or call the base class
	
	CPropertyPage::OnOK();
}

void CConnectDlg::OnButtonedittextfile() 
{
	CString filename;
	// Launch the file open dialog to get file name

	static char BASED_CODE szFilter[] = "Text Files (*.txt)|*.txt|";
	CFileDialog dlgFile(TRUE,NULL,NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter);
	if(dlgFile.DoModal() == IDOK)
	{
		filename = dlgFile.GetPathName();
		ShellExecute(NULL, _T("open"), _T("notepad.exe"),filename, NULL, SW_SHOWNORMAL);
	}		
}

void CConnectDlg::OnButtonloadaddress() 
{
	// Launch the file open dialog to get file name

	static char BASED_CODE szFilter[] = "Address Files (*.txt)|*.txt|";
	CFileDialog dlgFile(TRUE,NULL,NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,szFilter);
	if(dlgFile.DoModal() == IDOK)
	{
		szAddrFileName = dlgFile.GetPathName();
	}		
}
