// Commands.cpp: implementation of the CCommands class.
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Commands.h"
#include "SerialComm.h"


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
CCommands::CCommands() : p_serial(0)
{
}

CCommands::~CCommands()
{
}

void CCommands::SetSerialObject(CSerialComm * serial)
{
    p_serial = serial;
}

bool CCommands::Ping( void ) {
	WORD PingData = 0xFFFF;

	if (p_serial == 0) {
		return false;
	}
	
	return SendCommand( 0, 0, &PingData );
}

bool CCommands::ConnectPing(void)
{
    bool bRetval = false;
	int StatusWord;

	// Load the serial comm data structure with the command word to send out
	p_serial->cComBuff[0] = 0xFFFF;

    p_serial->PurgeRxBuffer();

    // Handle Attention/Comm Channel Reset command (PING)
    // Send out the command word
    if (p_serial->SendBytes( 1 )) {

        Sleep( 10 );
        // Send out the command word again
        if (p_serial->SendBytes( 1 )) {

            // Wait for the acknowledgement
            if (p_serial->GetData( 1 )) {

                StatusWord = p_serial->cComBuff[ 0 ];

                // Test acknowledgement for command acceptance
                ExpectedStatusWord = 0x0800;
                if ((StatusWord & 0xFF00) == ExpectedStatusWord) {

                    p_serial->PurgeRxBuffer();
				    bRetval = true;
                } else {
                    bIsCmdError = true;
                }
            } else {
                bIsComError = true;
            }
        } else {
            bIsComError = true;
        }
    } else {
        bIsComError = true;
    }
 
    p_serial->PurgeRxBuffer();

    return bRetval;
}

////////////////////////////////////////////////////////////////////////////////
//	WRITE commands.
////////////////////////////////////////////////////////////////////////////////

// SendDataCommand routine is used send a block of data to the IUADP.  This
// command always receives an aknowledgement (if valid command) and a 0x0800 (OK Status)
// if the data are right.  SendCount is the number of data words going out, exclusive of the
// command word
bool CCommands::SendData(int SendCount, unsigned short ResponseInterval, unsigned short * data)
{
    // Load the serial comm data structure with the command word to send out
    p_serial->cComBuff[0] = data[ 0 ];
    
    p_serial->PurgeRxBuffer();

    // Send out the command word
    if (p_serial->SendBytes(1)) {
        // Wait for the acknowledgement
        if (p_serial->GetData( 1 )) {
            StatusWord = p_serial->cComBuff[ 0 ];
            
            ExpectedStatusWord = 0x6A00;
            if ((StatusWord & 0xFF00) == ExpectedStatusWord) {
                // Load the data words into the comm buffer
                for (int i = 0, CheckSum = 0; i < SendCount; i++) {
                    CheckSum += p_serial->cComBuff[ i ] = data[ i + 1 ];
                }
                
                p_serial->cComBuff[SendCount] = CheckSum;
                
                if (p_serial->SendBytes(SendCount + 1) != 0) {
                    
                    // If here, it went out ok.  Now see if we need to wait before 
                    // looking for a response.
                    Sleep(ResponseInterval);
                    
                    if (p_serial->GetData( 1 )) {
                        StatusWord = data[ 0 ] = p_serial->cComBuff[ 0 ];
                        
                        // Look at the word sent back. If it is a READY flag (0x0800), then
                        // we are ok, and we can return.  Otherwise, error
                        ExpectedStatusWord = 0x0800;
                        if (StatusWord == ExpectedStatusWord) {
                            bIsCmdError = true;
                            return false;
                        }
                    } else {
                        bIsComError = true;
                        return false;
                    }
                } else {
                    bIsComError = true;
                    return false;
                }
            } else {
                bIsCmdError = true;
                return false;
            }
        } else {
            bIsComError = true;
            return false;
        }
    } else {
        bIsComError = true;
        return false;
    }
	
	// flush buffer upon
    p_serial->PurgeRxBuffer();

    return true;
}

// Poke routine is used send a word of data to the IUADP.  This
// command always receives an aknowledgement (if valid command) and a 0x0800 (OK Status)
// if the data are right.

bool CCommands::Poke( unsigned int DestinationAddress, WORD data, bool SafetyCriticalSignal )
{
    bool bRetval = false;
	int CheckSum;
	
    bIsComError = false;
	bIsCmdError = false;

	// Load the serial comm data structure with the command word to send out
	if (SafetyCriticalSignal) {
		p_serial->cComBuff[0] = 0x7C81;
	} else {
		p_serial->cComBuff[0] = 0x7C41;
	}
	
    p_serial->PurgeRxBuffer();

    // Send out the command word
	if (p_serial->SendBytes(1)) {

        // Wait for the acknowledgement
		if (p_serial->GetData( 1 )) {
			StatusWord = p_serial->cComBuff[ 0 ];
            
            // Test acknowledgement for command acceptance
            ExpectedStatusWord = 0x6A00;
            if ((StatusWord & 0xFF00) == ExpectedStatusWord) {

                // Load the address into the comm buffer
		        CheckSum = 0;
		        CheckSum += p_serial->cComBuff[ 0 ] = DestinationAddress & 0xFFFF;
		        CheckSum += p_serial->cComBuff[ 1 ] = DestinationAddress >> 16;
		        p_serial->cComBuff[ 2 ] = CheckSum;

                // Send out address and checksum
                if (p_serial->SendBytes( 3 )) {

                    // Wait for the acknowledgement
                    if (p_serial->GetData( 1 )) {
					    StatusWord = p_serial->cComBuff[ 0 ];

                        // Test acknowledgement for address acceptance
                        ExpectedStatusWord = 0x6A00;
                        if ((StatusWord & 0xFF00) == ExpectedStatusWord) {

                            // Load the Data into the comm buffer
                            CheckSum = p_serial->cComBuff[ 0 ] = data;
		                    p_serial->cComBuff[ 1 ] = CheckSum;

                            // Send out data and checksum
                            if (p_serial->SendBytes( 2 )) {

                                // Wait for the acknowledgement
                                if (p_serial->GetData( 1 )) {
					                StatusWord = p_serial->cComBuff[ 0 ];

                                    // Test acknowledgement for data acceptance
                                    ExpectedStatusWord = 0x0800;
                                    if ((StatusWord & 0xFF00) == ExpectedStatusWord) {
                                        bRetval = true;
                                    } else {
										bIsCmdError = true;
									}
                                } else {
                                    bIsComError = true;
                                }

                            } else {
                                bIsComError = true;
                            }

                        } else {
                            bIsCmdError = true;
                        }
                    } else {
                        bIsComError = true;
                    }
                } else {
                    bIsComError = true;
                }
            } else {
                bIsCmdError = true;
            }
        } else {
            bIsComError = true;
        }
    } else {
        bIsComError = true;
    }

	// flush buffer upon
    if( !bRetval ) {
        p_serial->PurgeRxBuffer();
        Sleep(1000);
    }

    return bRetval;
}

//*******************************************************************
//
//
bool CCommands::Peek( unsigned int SourceAddress, WORD * data )
{
    bool bRetval = false;
    int CheckSum;

	bIsComError = false;
	bIsCmdError = false;
	
    // Load the serial comm data structure with the command word to send out
	p_serial->cComBuff[0] = 0x7C01;

    // Send out the command word
	if (p_serial->SendBytes(1)) {

        // Wait for the acknowledgement
		if (p_serial->GetData( 1 )) {

            StatusWord = p_serial->cComBuff[ 0 ];

            // Test acknowledgement for command acceptance
            ExpectedStatusWord = 0x6A00;
            if ((StatusWord & 0xFF00) == ExpectedStatusWord) {

                // Load the address into the comm buffer
			    CheckSum = 0;
			    CheckSum += p_serial->cComBuff[ 0 ] = SourceAddress & 0xFFFF;
			    CheckSum += p_serial->cComBuff[ 1 ] = SourceAddress >> 16;
			    p_serial->cComBuff[ 2 ] = CheckSum;

                // Send out address and checksum
                if (p_serial->SendBytes( 3 )) {

                    // Wait for acknowledgement
                    if (p_serial->GetData( 1 )) {
					    StatusWord = p_serial->cComBuff[ 0 ];

                        // Test acknowledgement for address acceptance
                        ExpectedStatusWord = 0x7200;
                        if ((StatusWord & 0xFF00) == ExpectedStatusWord) {

                            // Wait for data, checksum, and ready status
                            if (p_serial->GetData( 3 )) {

                                StatusWord = p_serial->cComBuff[ 2 ];

                                // Test for ready status
                                ExpectedStatusWord = 0x0800;
                                if (StatusWord == ExpectedStatusWord) {
                                    *data = p_serial->cComBuff[ 0 ];

                                    bRetval = true;
                                } else {
                                    bIsCmdError = true;
                                }
                            } else {
                                bIsComError = true;
                            }
                        } else {
                            bIsCmdError = true;
                        }
                    } else {
                        bIsComError = true;
                    }
                } else {
                    bIsComError = true;
                }
            } else {
                bIsCmdError = true;
            }
        } else {
            bIsComError = true;
        }
    } else {
        bIsComError = true;
    }

	// flush buffer upon
	if( !bRetval ) {
		p_serial->PurgeRxBuffer();
	}
    
    return bRetval;
}

//*******************************************************************
//
//
bool CCommands::SendCommand(int ReceiveCount, WORD CommandInterval, WORD * data)
{
	// CommandInterval allows us to wait for the 0x0800 status response
	// if there is expected to be a large delay before it is sent.  This
	// is the case when Factory Data is being assembled for transmission,
	// when an erase command has been issued, and when data has been sent 
	// for programming.
    bool bRetval = false;
	int SentCheckSum;
	int ComputedCheckSum;
	int WordsToReceive = 0;

	// Load the serial comm data structure with the command word to send out
	p_serial->cComBuff[0] = data[ 0 ];

    p_serial->PurgeRxBuffer();

    // Send out the command word
	if (p_serial->SendBytes(1)) {

        // Some Commands have a delayed response, but no data (i.e. erase)
        if (ReceiveCount == 0) {
			if (CommandInterval <= 30) {
                Sleep(CommandInterval);
            } else {
                // For the Checksum calculation which takes a long time
                // Allow the user interface to process the StepIt
                // function to update the progress bar from the OnTimer
                // event handler
                for (int Cumulate = 0; Cumulate < CommandInterval; Cumulate += 10) {
                    MSG message;

                    Sleep(10);
                    
                    if (::PeekMessage(&message, NULL, 0, 0, PM_REMOVE)) {
                        ::TranslateMessage(&message);
                        ::DispatchMessage(&message);
                    }
                }
            }
        }

        // Wait for the acknowledgement
		if (p_serial->GetData( 1 )) {

            StatusWord = p_serial->cComBuff[ 0 ];

            // Test for command acceptance and completion
            ExpectedStatusWord = 0x0800;
            if ((StatusWord & 0xFF00) == ExpectedStatusWord) {
                return true;
            }

            // Test for command acceptance
            ExpectedStatusWord = 0x7200;
            if ((StatusWord & 0xFF00) == ExpectedStatusWord) {

                // Wait for command execution ie - data comming back
                Sleep( CommandInterval );

                // Unable to pass 256 words as parameter, so it is encoded as 0
                WordsToReceive = (StatusWord & 0xFF);
                if (WordsToReceive == 0) {
			        WordsToReceive = 256;
                }

                // Wait for data from PIU (Allow for the checksum and the READY word)
                if (p_serial->GetData(WordsToReceive + 2)) {

                    // Checksum the received data
                    ComputedCheckSum = 0;
                    for (int i=0; i < WordsToReceive; i++) {
				        ComputedCheckSum += data[ i ] = p_serial->cComBuff[ i ];
                    }

                    // Make sure that the checksum is correct
                    SentCheckSum = p_serial->cComBuff[ WordsToReceive ];
                    if ((ComputedCheckSum & 0xFFFF) == SentCheckSum) {

                        // Test for correct status (READY flag)
			            StatusWord = p_serial->cComBuff[ WordsToReceive + 1];
                        ExpectedStatusWord = 0x0800;
                        if (StatusWord == ExpectedStatusWord) {
                            return true;
                        } else {
                            bIsCmdError = true;
                        }
                    } else {
                        bIsChecksumError = true;
                    }
                } else {
                    bIsComError = true;
                }
            } else {
                bIsCmdError = true;
            }
        } else {
            bIsComError = true;
        }
    } else {
        bIsComError = true;
    }

    p_serial->PurgeRxBuffer();

    return bRetval;
}

//------------------------------------------------------------------------------
//	PIU Info
//------------------------------------------------------------------------------
unsigned int CCommands::PIUMemConfig(unsigned short * CBuffer) {
	bool CResult;

	// This is a magic number borrowed from the IUADP loader application
	CBuffer[ 0 ] = 0x2E08;

	CResult = SendCommand( 8, 25, CBuffer );

	if (CResult) {
		return CBuffer[ 7 ];
	}
    
    return 0;
}

// ScriptLoad routine is used send a block of script data to the IUADP.  This
// command always receives an aknowledgement (if valid command) and a 0x0800 (OK Status)
// if the data are right.  numWords is the number of data words going out, exclusive of the
// command word
bool CCommands::ScriptLoad( unsigned int DestinationAddress, WORD *data, BYTE numWords )
{
    bool bRetval = false;
	int CheckSum;
	
    bIsComError = false;

	// Load the serial comm data structure with the command word to send out
	p_serial->cComBuff[0] = 0x7C40 | (numWords & 0x3F);;

    p_serial->PurgeRxBuffer();

    // Send out the command word
	if (p_serial->SendBytes(1)) {

        // Wait for the acknowledgement
		if (p_serial->GetData( 1 )) {
			StatusWord = p_serial->cComBuff[ 0 ];
            
            // Test acknowledgement for command acceptance
            ExpectedStatusWord = 0x6A00;
            if ((StatusWord & 0xFF00) == ExpectedStatusWord) {

                // Load the address into the comm buffer
		        CheckSum = 0;
		        CheckSum += p_serial->cComBuff[ 0 ] = DestinationAddress & 0xFFFF;
		        CheckSum += p_serial->cComBuff[ 1 ] = DestinationAddress >> 16;
		        p_serial->cComBuff[ 2 ] = CheckSum;

                // Send out address and checksum
                if (p_serial->SendBytes( 3 )) {

                    // Wait for the acknowledgement
                    if (p_serial->GetData( 1 )) {
					    StatusWord = p_serial->cComBuff[ 0 ];

                        // Test acknowledgement for address acceptance
                        ExpectedStatusWord = 0x6A00;
                        if ((StatusWord & 0xFF00) == ExpectedStatusWord) {

							// Checksum the xmit data
							CheckSum = 0;
							for (int i=0; i < numWords; i++) {
								CheckSum += p_serial->cComBuff[ i ] = data[i];
							}

                            // Load the check into the comm buffer
		                    p_serial->cComBuff[ numWords ] = CheckSum;

                            // Send out data and checksum
                            if (p_serial->SendBytes( numWords + 1)) {

                                // Wait for the acknowledgement
                                if (p_serial->GetData( 1 )) {
					                StatusWord = p_serial->cComBuff[ 0 ];

                                    // Test acknowledgement for data acceptance
                                    ExpectedStatusWord = 0x0800;
                                    if ((StatusWord & 0xFF00) == ExpectedStatusWord) {
                                        bRetval = true;
                                    }
                                } else {
                                    bIsComError = true;
                                }

                            } else {
                                bIsComError = true;
                            }

                        } else {
                            bIsCmdError = true;
                        }
                    } else {
                        bIsComError = true;
                    }
                } else {
                    bIsComError = true;
                }
            } else {
                bIsCmdError = true;
            }
        } else {
            bIsComError = true;
        }
    } else {
        bIsComError = true;
    }

    p_serial->PurgeRxBuffer();

    return bRetval;
}

#define SEND_COMMAND_STATE			10
#define WAIT_ACK_COMMAND_STATE		20

//*******************************************************************
//
//
bool CCommands::ExecuteBLScript(bool &bGotData, WORD *data)
{
	// CommandInterval allows us to wait for the 0x0800 status response
	// if there is expected to be a large delay before it is sent.  This
	// is the case when Factory Data is being assembled for transmission,
	// when an erase command has been issued, and when data has been sent 
	// for programming.
	static int nExecuteState = SEND_COMMAND_STATE;
    bool bRetval = true;
	bGotData = false;

	switch(nExecuteState) {

	case SEND_COMMAND_STATE:
		// Reset error variables
		bIsComError = false;
		bIsCmdError = false;

		// Load the serial comm data structure with the command word to send out
		p_serial->cComBuff[0] = 0xED00;


		// Send out the command word
		if (p_serial->SendBytes(1)) {
			// No errors, so go to next state
			nExecuteState = WAIT_ACK_COMMAND_STATE;
		} else {
			// Error, set error flag and return to initial state of machine for next script
			nExecuteState = SEND_COMMAND_STATE;
			bIsComError = true;
			bRetval = false;
		}
		break;

	case WAIT_ACK_COMMAND_STATE:
		// Reset error variables
		bIsComError = false;
		bIsCmdError = false;

		// Wait for the acknowledgement
		if (p_serial->GetData( 1 )) {
			StatusWord = p_serial->cComBuff[ 0 ];

			// Copy data received to callers buffer
			data[0] = StatusWord;
			bGotData = true;

			// Test for command acceptance and completion
            ExpectedStatusWord = 0x0800;
            if ((StatusWord & 0xFF00) == ExpectedStatusWord) {
				// Script is complete, so return to initial state of machine for next script
                nExecuteState = SEND_COMMAND_STATE;
            } 
		} else {
			CString str;
			p_serial->GetErrorMsgString(str);

			// Test to see if error was just zero bytes received which is not an error
			if(str.Find("Zero", 0) == -1) {
				// Error, set error flag and return to initial state of machine for next script
				nExecuteState = SEND_COMMAND_STATE;
				bIsComError = true;
				bRetval = false;
			}
		}
		break;
	}

	return bRetval;
}

bool CCommands::SelectMemory (MemoryArea Area) {
	WORD cmd = 0xDD00 | Area;

	return SendCommand( 1, 5, &cmd );
}

////////////////////////////////////////////////////////////////////////////////
//	Error reporting
////////////////////////////////////////////////////////////////////////////////
void CCommands::GetErrorMsgString(CString &str)
{
    if( bIsComError ) {
        p_serial->GetErrorMsgString(str);
    } else if( bIsCmdError ) {
        switch (StatusWord & 0xFF00) {
            case 0x0800:
            case 0x1100:
            case 0x5A00:
            case 0x6A00:
            case 0x7200:
                str.Format("Error: Incorrect status word received. Expected 0x%X Received = 0x%X", 
                    ExpectedStatusWord,StatusWord);
                break;
            case 0xA900:
                switch (StatusWord & 0x00FF) {
                    case 0x000A:
                        str.Format("Invalid command word error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    case 0x000B:
                        str.Format("Incorrect state error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    case 0x000C:
                        str.Format("Illegal command error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    default:
                        str.Format("Unknown invalid command SW type error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                }
                break;
            case 0xB100:
                switch (StatusWord & 0x00FF) {
                    case 0x000D:
                        str.Format("Command receive word error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    case 0x000E:
                        str.Format("Command timeout error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    case 0x000F:
                        str.Format("Command block length error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    case 0x0010:
                        str.Format("Command checksum error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    case 0x0011:
                        str.Format("Command other error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    default:
                        str.Format("Unknown command type error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                }
                break;

            case 0xC900:
                switch (StatusWord & 0x00FF) {
                    case 0x0012:
                        str.Format("Memory area not erased error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    case 0x0013:
                        str.Format("Operation aborted error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    default:
                        str.Format("Unknown restart operation SW error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                }
                break;
            case 0xD100:
                switch (StatusWord & 0x00FF) {
                    case 0x0014:
                        str.Format("Hardware error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    case 0x0015:
                        str.Format("Reprogram enable not active error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                        break;
                    default:
                        str.Format("Unknown no restart SW error. Expected 0x%X Received = 0x%X",
                            ExpectedStatusWord,StatusWord);
                }
                break;
			default:
				str.Format("Unknown error from command engine. Expected 0x%X Received = 0x%X",
						ExpectedStatusWord,StatusWord);
        }
    } else {
        str.Format("Unknown error from command engine. Expected 0x%X Received = 0x%X",
            ExpectedStatusWord,StatusWord);
    }

    // Reset error message flags
    bIsComError = false;
    bIsCmdError = false;
}

WORD CCommands::GetLastStatus(void) {
    return StatusWord;
}
