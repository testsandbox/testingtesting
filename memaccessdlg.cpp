// MemAccessDlg.cpp : implementation file
//

#include "stdafx.h"
#include "XAG49_Toolbox.h"
#include "MemAccessDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMemAccessDlg property page

IMPLEMENT_DYNCREATE(CMemAccessDlg, CPropertyPage)

CMemAccessDlg::CMemAccessDlg() : CPropertyPage(CMemAccessDlg::IDD)
{
	//{{AFX_DATA_INIT(CMemAccessDlg)
	m_Status = _T("");
	//}}AFX_DATA_INIT
}

CMemAccessDlg::~CMemAccessDlg()
{
}

void CMemAccessDlg::DoDataExchange(CDataExchange* pDX)
{
	CPropertyPage::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMemAccessDlg)
	DDX_Text(pDX, IDC_EDITSTATUSACCESS, m_Status);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMemAccessDlg, CPropertyPage)
	//{{AFX_MSG_MAP(CMemAccessDlg)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_BUTTONCONTREAD, OnButtoncontread)
	ON_BN_CLICKED(IDC_BUTTONCONTSTOP, OnButtoncontstop)
	ON_BN_CLICKED(IDC_CHECKSTOPERROR, OnCheckstoperror)
	ON_BN_CLICKED(IDC_MEMREADBTN1, OnDynamicRead)
	ON_BN_CLICKED(IDC_MEMREADBTN2, OnDynamicRead2)
	ON_BN_CLICKED(IDC_MEMREADBTN3, OnDynamicRead3)
	ON_BN_CLICKED(IDC_MEMREADBTN4, OnDynamicRead4)
	ON_BN_CLICKED(IDC_MEMREADBTN5, OnDynamicRead5)
	ON_BN_CLICKED(IDC_MEMREADBTN6, OnDynamicRead6)
	ON_BN_CLICKED(IDC_MEMREADBTN7, OnDynamicRead7)
	ON_BN_CLICKED(IDC_MEMREADBTN8, OnDynamicRead8)
	ON_BN_CLICKED(IDC_MEMREADBTN9, OnDynamicRead9)
	ON_BN_CLICKED(IDC_MEMREADBTN10, OnDynamicRead10)
	ON_BN_CLICKED(IDC_MEMREADBTN11, OnDynamicRead11)
	ON_BN_CLICKED(IDC_MEMREADBTN12, OnDynamicRead12)
	ON_BN_CLICKED(IDC_MEMREADBTN13, OnDynamicRead13)
	ON_BN_CLICKED(IDC_MEMREADBTN14, OnDynamicRead14)
	ON_BN_CLICKED(IDC_MEMREADBTN15, OnDynamicRead15)
	ON_BN_CLICKED(IDC_MEMREADBTN16, OnDynamicRead16)
	ON_BN_CLICKED(IDC_MEMREADBTN17, OnDynamicRead17)
	ON_BN_CLICKED(IDC_MEMREADBTN18, OnDynamicRead18)
	ON_BN_CLICKED(IDC_MEMWRITEBTN1, OnDynamicWrite)
	ON_BN_CLICKED(IDC_MEMWRITEBTN2, OnDynamicWrite2)
	ON_BN_CLICKED(IDC_MEMWRITEBTN3, OnDynamicWrite3)
	ON_BN_CLICKED(IDC_MEMWRITEBTN4, OnDynamicWrite4)
	ON_BN_CLICKED(IDC_MEMWRITEBTN5, OnDynamicWrite5)
	ON_BN_CLICKED(IDC_MEMWRITEBTN6, OnDynamicWrite6)
	ON_BN_CLICKED(IDC_MEMWRITEBTN7, OnDynamicWrite7)
	ON_BN_CLICKED(IDC_MEMWRITEBTN8, OnDynamicWrite8)
	ON_BN_CLICKED(IDC_MEMWRITEBTN9, OnDynamicWrite9)
	ON_BN_CLICKED(IDC_MEMWRITEBTN10, OnDynamicWrite10)
	ON_BN_CLICKED(IDC_MEMWRITEBTN11, OnDynamicWrite11)
	ON_BN_CLICKED(IDC_MEMWRITEBTN12, OnDynamicWrite12)
	ON_BN_CLICKED(IDC_MEMWRITEBTN13, OnDynamicWrite13)
	ON_BN_CLICKED(IDC_MEMWRITEBTN14, OnDynamicWrite14)
	ON_BN_CLICKED(IDC_MEMWRITEBTN15, OnDynamicWrite15)
	ON_BN_CLICKED(IDC_MEMWRITEBTN16, OnDynamicWrite16)
	ON_BN_CLICKED(IDC_MEMWRITEBTN17, OnDynamicWrite17)
	ON_BN_CLICKED(IDC_MEMWRITEBTN18, OnDynamicWrite18)
	ON_WM_VSCROLL()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMemAccessDlg message handlers

BOOL CMemAccessDlg::OnInitDialog() 
{
	CPropertyPage::OnInitDialog();
	
	// Setup PIU Memory Read/Write Area
	SetUpMemAccessControls(0);
	CScrollBar *pBar = (CScrollBar *)GetDlgItem(IDC_SCROLLBAR1);
	pBar->SetScrollRange(0,12);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CMemAccessDlg::OnSetActive() 
{
	return CPropertyPage::OnSetActive();
}

void CMemAccessDlg::OnTimer(UINT nIDEvent) 
{
	CString address;
	CString data;
	int i, nIDMemType, nIDAddress, nIDDataVal;

    switch (nIDEvent) {

	case PEEKPOKE_LOOP_TIMER:
		// Stop Timer Until Finished with action
		KillTimer(PEEKPOKE_LOOP_TIMER);

		i = nNextGroupNumberToLoop;

		// Compute control ID numbers
		nIDMemType = 2000 + (i*7);
		nIDAddress = 2001 + (i*7);
		nIDDataVal = 2002 + (i*7);

		// Process read button if selected
		if(ReadControlsToLoop[i]) {
			Read(nIDAddress, nIDMemType, data);
		}

		// Stop looping if error detected and stop on error selected
		if(bStopOnError && !bErrorDetected) {
			break;
		}

		// Process write button if selected
		if(WriteControlsToLoop[i]) {
			GetDlgItemText(nIDDataVal, data);
			Write(nIDAddress, nIDMemType, data);
		}

		// Stop looping if error detected and stop on error selected
		if(bStopOnError && !bErrorDetected) {
			break;
		}

		// Find next control to loop
		nNextGroupNumberToLoop++;
		if(nNextGroupNumberToLoop >= NUM_CONTROL_GROUPS) {
			nNextGroupNumberToLoop = 0;
		}

		while ( (!ReadControlsToLoop[nNextGroupNumberToLoop]) && (!WriteControlsToLoop[nNextGroupNumberToLoop]) ) {
			nNextGroupNumberToLoop++;
			if(nNextGroupNumberToLoop >= NUM_CONTROL_GROUPS) {
				nNextGroupNumberToLoop = 0;
			}
		}

		// Reschedule timer to process next control
		SetTimer(PEEKPOKE_LOOP_TIMER,nDelay,NULL);

		// Increment loop counter and update display
        nLoopCount++;
        data.Format("%d",nLoopCount);
        SetDlgItemText(IDC_EDITLOOP,data);
        UpdateData(FALSE);
	}
	
	CPropertyPage::OnTimer(nIDEvent);
}

void CMemAccessDlg::OnButtoncontread() 
{
	CString str;

	// Initialize Loop Count
	nLoopCount = 0;
	bErrorDetected = false;

	// Determine Controls to Loop
	MakeControlLoopList();

	// Start loop timer
	if(nNextGroupNumberToLoop != -1) {
		GetDlgItemText(IDC_EDITLOOPDELAY,str);
		nDelay = atoi((LPCTSTR)str);
		if(nDelay <= 0) {
			MessageBox("No loop delay selected");
		} else if(nDelay > 0xFFFF) {
			MessageBox("Enter a loop delay less than 65535 msec");
		} else {
			SetTimer(PEEKPOKE_LOOP_TIMER,50,NULL);
		}
	} else {
		MessageBox("No controls selected to loop");
	}
}

void CMemAccessDlg::OnButtoncontstop() 
{
	// Kill Timer
    KillTimer(PEEKPOKE_LOOP_TIMER);	
}

void CMemAccessDlg::OnCheckstoperror() 
{
	CButton *pBtn = (CButton *)GetDlgItem(IDC_CHECKSTOPERROR);
	if (pBtn->GetCheck()) {
		bStopOnError = true;
	} else {
		bStopOnError = false;
	}	
}

void CMemAccessDlg::OnVScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
{
	/*
	SB_BOTTOM   Scroll to bottom. 
	SB_ENDSCROLL   End scroll. 
	SB_LINEDOWN   Scroll one line down. 
	SB_LINEUP   Scroll one line up. 
	SB_PAGEDOWN   Scroll one page down. 
	SB_PAGEUP   Scroll one page up. 
	SB_THUMBPOSITION   Scroll to the absolute position. The current position is provided in nPos. 
	SB_THUMBTRACK   Drag scroll box to specified position. The current position is provided in nPos. 
	SB_TOP   Scroll to top.
	*/

	int nCurPos = pScrollBar->GetScrollPos();

	switch(nSBCode) {

	case SB_LINEDOWN:
	case SB_PAGEDOWN:
		SetUpMemAccessControls(nCurPos+1);
		pScrollBar->SetScrollPos(nCurPos+1);
		break;

	case SB_LINEUP:
	case SB_PAGEUP:
		SetUpMemAccessControls(nCurPos-1);
		pScrollBar->SetScrollPos(nCurPos-1);
		break;

	case SB_THUMBPOSITION:
	case SB_THUMBTRACK:
		pScrollBar->SetScrollPos(nPos);
		SetUpMemAccessControls(nPos);
		break;

	}
	
	CPropertyPage::OnVScroll(nSBCode, nPos, pScrollBar);
}

void CMemAccessDlg::OnDynamicRead()
{
	//MessageBox("You pressed the read button 1");
	CString data = "";
	Read(GetAddressControlID(0), GetMemTypeControlID(0), data);
	SetDlgItemText(GetDataValueControlID(0),data);
}

void CMemAccessDlg::OnDynamicRead2()
{
	//MessageBox("You pressed the read button 2");
	CString data = "";
	Read(GetAddressControlID(1), GetMemTypeControlID(1), data);
	SetDlgItemText(GetDataValueControlID(1),data);
}

void CMemAccessDlg::OnDynamicRead3()
{
	//MessageBox("You pressed the read button 3");
	CString data = "";
	Read(GetAddressControlID(2), GetMemTypeControlID(2), data);
	SetDlgItemText(GetDataValueControlID(2),data);
}

void CMemAccessDlg::OnDynamicRead4()
{
	//MessageBox("You pressed the read button 4");
	CString data = "";
	Read(GetAddressControlID(3), GetMemTypeControlID(3), data);
	SetDlgItemText(GetDataValueControlID(3),data);
}

void CMemAccessDlg::OnDynamicRead5()
{
	//MessageBox("You pressed the read button 5");
	CString data = "";
	Read(GetAddressControlID(4), GetMemTypeControlID(4), data);
	SetDlgItemText(GetDataValueControlID(4),data);
}

void CMemAccessDlg::OnDynamicRead6()
{
	//MessageBox("You pressed the read button 6");
	CString data = "";
	Read(GetAddressControlID(5), GetMemTypeControlID(5), data);
	SetDlgItemText(GetDataValueControlID(5),data);
}

void CMemAccessDlg::OnDynamicRead7()
{
	//MessageBox("You pressed the read button 7");
	CString data = "";
	Read(GetAddressControlID(6), GetMemTypeControlID(6), data);
	SetDlgItemText(GetDataValueControlID(6),data);
}

void CMemAccessDlg::OnDynamicRead8()
{
	//MessageBox("You pressed the read button 8");
	CString data = "";
	Read(GetAddressControlID(7), GetMemTypeControlID(7), data);
	SetDlgItemText(GetDataValueControlID(7),data);
}

void CMemAccessDlg::OnDynamicRead9()
{
	//MessageBox("You pressed the read button 9");
	CString data = "";
	Read(GetAddressControlID(8), GetMemTypeControlID(8), data);
	SetDlgItemText(GetDataValueControlID(8),data);
}

void CMemAccessDlg::OnDynamicRead10()
{
	//MessageBox("You pressed the read button 10");
	CString data = "";
	Read(GetAddressControlID(9), GetMemTypeControlID(9), data);
	SetDlgItemText(GetDataValueControlID(9),data);
}

void CMemAccessDlg::OnDynamicRead11()
{
	//MessageBox("You pressed the read button 11");
	CString data = "";
	Read(GetAddressControlID(10), GetMemTypeControlID(10), data);
	SetDlgItemText(GetDataValueControlID(10),data);
}

void CMemAccessDlg::OnDynamicRead12()
{
	//MessageBox("You pressed the read button 12");
	CString data = "";
	Read(GetAddressControlID(11), GetMemTypeControlID(11), data);
	SetDlgItemText(GetDataValueControlID(11),data);
}

void CMemAccessDlg::OnDynamicRead13()
{
	//MessageBox("You pressed the read button 13");
	CString data = "";
	Read(GetAddressControlID(12), GetMemTypeControlID(12), data);
	SetDlgItemText(GetDataValueControlID(12),data);
}

void CMemAccessDlg::OnDynamicRead14()
{
	//MessageBox("You pressed the read button 14");
	CString data = "";
	Read(GetAddressControlID(13), GetMemTypeControlID(13), data);
	SetDlgItemText(GetDataValueControlID(13),data);
}

void CMemAccessDlg::OnDynamicRead15()
{
	//MessageBox("You pressed the read button 15");
	CString data = "";
	Read(GetAddressControlID(14), GetMemTypeControlID(14), data);
	SetDlgItemText(GetDataValueControlID(14),data);
}

void CMemAccessDlg::OnDynamicRead16()
{
	//MessageBox("You pressed the read button 16");
	CString data = "";
	Read(GetAddressControlID(15), GetMemTypeControlID(15), data);
	SetDlgItemText(GetDataValueControlID(15),data);
}

void CMemAccessDlg::OnDynamicRead17()
{
	//MessageBox("You pressed the read button 17");
	CString data = "";
	Read(GetAddressControlID(16), GetMemTypeControlID(16), data);
	SetDlgItemText(GetDataValueControlID(16),data);
}

void CMemAccessDlg::OnDynamicRead18()
{
	//MessageBox("You pressed the read button 18");
	CString data = "";
	Read(GetAddressControlID(17), GetMemTypeControlID(17), data);
	SetDlgItemText(GetDataValueControlID(17),data);
}

void CMemAccessDlg::OnDynamicWrite()
{
	//MessageBox("You pressed the write button 1");
	CString data;
	GetDlgItemText(GetDataValueControlID(0),data);
	Write(GetAddressControlID(0), GetMemTypeControlID(0), data);
}

void CMemAccessDlg::OnDynamicWrite2()
{
	//MessageBox("You pressed the write button 2");
	CString data;
	GetDlgItemText(GetDataValueControlID(1),data);
	Write(GetAddressControlID(1), GetMemTypeControlID(1), data);
}

void CMemAccessDlg::OnDynamicWrite3()
{
	//MessageBox("You pressed the write button 3");
	CString data;
	GetDlgItemText(GetDataValueControlID(2),data);
	Write(GetAddressControlID(2), GetMemTypeControlID(2), data);
}

void CMemAccessDlg::OnDynamicWrite4()
{
	//MessageBox("You pressed the write button 4");
	CString data;
	GetDlgItemText(GetDataValueControlID(3),data);
	Write(GetAddressControlID(3), GetMemTypeControlID(3), data);
}

void CMemAccessDlg::OnDynamicWrite5()
{
	//MessageBox("You pressed the write button 5");
	CString data;
	GetDlgItemText(GetDataValueControlID(4),data);
	Write(GetAddressControlID(4), GetMemTypeControlID(4), data);
}

void CMemAccessDlg::OnDynamicWrite6()
{
	//MessageBox("You pressed the write button 6");
	CString data;
	GetDlgItemText(GetDataValueControlID(5),data);
	Write(GetAddressControlID(5), GetMemTypeControlID(5), data);
}

void CMemAccessDlg::OnDynamicWrite7()
{
	//MessageBox("You pressed the write button 7");
	CString data;
	GetDlgItemText(GetDataValueControlID(6),data);
	Write(GetAddressControlID(6), GetMemTypeControlID(6), data);
}

void CMemAccessDlg::OnDynamicWrite8()
{
	//MessageBox("You pressed the write button 8");
	CString data;
	GetDlgItemText(GetDataValueControlID(7),data);
	Write(GetAddressControlID(7), GetMemTypeControlID(7), data);
}

void CMemAccessDlg::OnDynamicWrite9()
{
	//MessageBox("You pressed the write button 9");
	CString data;
	GetDlgItemText(GetDataValueControlID(8),data);
	Write(GetAddressControlID(8), GetMemTypeControlID(8), data);
}

void CMemAccessDlg::OnDynamicWrite10()
{
	//MessageBox("You pressed the write button 10");
	CString data;
	GetDlgItemText(GetDataValueControlID(9),data);
	Write(GetAddressControlID(9), GetMemTypeControlID(9), data);
}

void CMemAccessDlg::OnDynamicWrite11()
{
	//MessageBox("You pressed the write button 11");
	CString data;
	GetDlgItemText(GetDataValueControlID(10),data);
	Write(GetAddressControlID(10), GetMemTypeControlID(10), data);
}

void CMemAccessDlg::OnDynamicWrite12()
{
	//MessageBox("You pressed the write button 12");
	CString data;
	GetDlgItemText(GetDataValueControlID(11),data);
	Write(GetAddressControlID(11), GetMemTypeControlID(11), data);
}

void CMemAccessDlg::OnDynamicWrite13()
{
	//MessageBox("You pressed the write button 13");
	CString data;
	GetDlgItemText(GetDataValueControlID(12),data);
	Write(GetAddressControlID(12), GetMemTypeControlID(12), data);
}

void CMemAccessDlg::OnDynamicWrite14()
{
	//MessageBox("You pressed the write button 14");
	CString data;
	GetDlgItemText(GetDataValueControlID(13),data);
	Write(GetAddressControlID(13), GetMemTypeControlID(13), data);
}

void CMemAccessDlg::OnDynamicWrite15()
{
	//MessageBox("You pressed the write button 15");
	CString data;
	GetDlgItemText(GetDataValueControlID(14),data);
	Write(GetAddressControlID(14), GetMemTypeControlID(14), data);
}

void CMemAccessDlg::OnDynamicWrite16()
{
	//MessageBox("You pressed the write button 16");
	CString data;
	GetDlgItemText(GetDataValueControlID(15),data);
	Write(GetAddressControlID(15), GetMemTypeControlID(15), data);
}

void CMemAccessDlg::OnDynamicWrite17()
{
	//MessageBox("You pressed the write button 17");
	CString data;
	GetDlgItemText(GetDataValueControlID(16),data);
	Write(GetAddressControlID(16), GetMemTypeControlID(16), data);
}

void CMemAccessDlg::OnDynamicWrite18()
{
	//MessageBox("You pressed the write button 18");
	CString data;
	GetDlgItemText(GetDataValueControlID(17),data);
	Write(GetAddressControlID(17), GetMemTypeControlID(17), data);
}
///////////////////////////////////////////////////////////////////////////////
// Helper Functions
///////////////////////////////////////////////////////////////////////////////
UINT CMemAccessDlg::GetMemTypeControlID(int nControlGroupNumber)
{
	return ( 2000 + (nControlGroupNumber*7) );
}

UINT CMemAccessDlg::GetAddressControlID(int nControlGroupNumber)
{
	return ( 2001 + (nControlGroupNumber*7) );
}

UINT CMemAccessDlg::GetDataValueControlID(int nControlGroupNumber)
{
	return ( 2002 + (nControlGroupNumber*7) );
}

// This function creates 18 sets of Read Write memory access controls the first
// time it is called. Then it displays 6 sets of controls starting with control
// set number nFirstControlNum
void CMemAccessDlg::SetUpMemAccessControls(int nFirstControlNum)
{
	static bool bHaveCreatedControls = false;
	int i, nHeight, nWidth;
	static CRect SaveRect;
	CRect AreaRect, AddrRect, TypeRect, ValueRect, ReadRect, WriteRect, RdEnRect, WrEnRect, TmpRect;;
	CStatic *pStatic;
	CString str;

	if(!bHaveCreatedControls)
	{
		this->GetClientRect(&AreaRect);
		TypeRect.top = AreaRect.top + 100;
		TypeRect.bottom = TypeRect.top + 70;
		TypeRect.left = AreaRect.left + 60;
		TypeRect.right = TypeRect.left +120;
		SaveRect = TypeRect;
		
		for(i=0; i < NUM_CONTROL_GROUPS; i++) {

			AddrRect = ValueRect = ReadRect = WriteRect = RdEnRect = WrEnRect =TypeRect;

			Controls[i].Type.Create(WS_CHILD|WS_VISIBLE|CBS_DROPDOWN|WS_VSCROLL|WS_TABSTOP,
				TypeRect, this, 2000 + (i*7));
			LoadMemoryAreaData(2000 + (i*7));

			AddrRect.left = TypeRect.right + 5;
			AddrRect.bottom = AddrRect.top + 70;
			AddrRect.right = AddrRect.left + 70;
			Controls[i].Address.Create(WS_CHILD|WS_VISIBLE|CBS_DROPDOWN|WS_VSCROLL|WS_TABSTOP,
				AddrRect, this, 2001 + (i*7));
			LoadPIUAddressData(szAddrFileName, 2001 + (i*7));

			ValueRect.left = AddrRect.right + 5;
			ValueRect.bottom = ValueRect.top + 21;
			ValueRect.right = ValueRect.left + 70;
			Controls[i].Data.Create(WS_CHILD|WS_VISIBLE|ES_AUTOHSCROLL|WS_BORDER,
				ValueRect, this, 2002 + (i*7));

			ReadRect.left = ValueRect.right + 5;
			ReadRect.bottom = ReadRect.top + 21;
			ReadRect.right = ReadRect.left + 40;
			Controls[i].ReadBtn.Create("Rd",WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON,
				ReadRect, this, 2003 + (i*7));

			WriteRect.left = ReadRect.right + 5;
			WriteRect.bottom = WriteRect.top + 21;
			WriteRect.right = WriteRect.left + 40;
			Controls[i].WriteBtn.Create("Wr",WS_CHILD|WS_VISIBLE|BS_PUSHBUTTON,
				WriteRect, this, 2004 + (i*7));

			RdEnRect.left = WriteRect.right + 5;
			RdEnRect.bottom = RdEnRect.top + 25;
			RdEnRect.right = RdEnRect.left + 60;
			Controls[i].RdEnBtn.Create("RdEn",WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX,
				RdEnRect, this, 2005 + (i*7));

			WrEnRect.left = RdEnRect.right + 5;
			WrEnRect.bottom = WrEnRect.top + 25;
			WrEnRect.right = WrEnRect.left + 60;
			Controls[i].WrEnBtn.Create("WrEn",WS_CHILD|WS_VISIBLE|BS_AUTOCHECKBOX,
				WrEnRect, this, 2006 + (i*7));

			// Only 6 controls visable at a time
			if( (i==5) || (i==11) ) {
				TypeRect = SaveRect;
			} else {
				TypeRect.top += 40;
				TypeRect.bottom += 40;
			}
		}

		bHaveCreatedControls = true;
	}

	// Disable all controls
	for(i=0; i<NUM_CONTROL_GROUPS; i++) {
		Controls[i].Type.ShowWindow(SW_HIDE);
		Controls[i].Address.ShowWindow(SW_HIDE);
		Controls[i].Data.ShowWindow(SW_HIDE);
		Controls[i].ReadBtn.ShowWindow(SW_HIDE);
		Controls[i].WriteBtn.ShowWindow(SW_HIDE);
		Controls[i].RdEnBtn.ShowWindow(SW_HIDE);
		Controls[i].WrEnBtn.ShowWindow(SW_HIDE);
	}

	TypeRect = SaveRect;

	// Test for valid starting control group number

	if(nFirstControlNum > 12) {
		nFirstControlNum = 12;
	} else if(nFirstControlNum < 0) {
		nFirstControlNum = 0;
	}

	for(i=nFirstControlNum; i < nFirstControlNum + 6; i++) {

		AddrRect = ValueRect = ReadRect = WriteRect = RdEnRect = WrEnRect = TypeRect;
		Controls[i].Type.MoveWindow(&TypeRect);
		Controls[i].Type.ShowWindow(SW_SHOWNORMAL);

		AddrRect.left = TypeRect.right + 5;
		AddrRect.bottom = AddrRect.top + 70;
		AddrRect.right = AddrRect.left + 70;
		Controls[i].Address.MoveWindow(&AddrRect);
		Controls[i].Address.ShowWindow(SW_SHOWNORMAL);

		ValueRect.left = AddrRect.right + 5;
		ValueRect.bottom = ValueRect.top + 21;
		ValueRect.right = ValueRect.left + 70;
		Controls[i].Data.MoveWindow(&ValueRect);
		Controls[i].Data.ShowWindow(SW_SHOWNORMAL);

		ReadRect.left = ValueRect.right + 5;
		ReadRect.bottom = ReadRect.top + 21;
		ReadRect.right = ReadRect.left + 40;
		Controls[i].ReadBtn.MoveWindow(&ReadRect);
		Controls[i].ReadBtn.ShowWindow(SW_SHOWNORMAL);

		WriteRect.left = ReadRect.right + 5;
		WriteRect.bottom = WriteRect.top + 21;
		WriteRect.right = WriteRect.left + 40;
		Controls[i].WriteBtn.MoveWindow(&WriteRect);
		Controls[i].WriteBtn.ShowWindow(SW_SHOWNORMAL);

		RdEnRect.left = WriteRect.right + 5;
		RdEnRect.bottom = RdEnRect.top + 25;
		RdEnRect.right = RdEnRect.left + 60;
		Controls[i].RdEnBtn.ShowWindow(SW_SHOWNORMAL);

		WrEnRect.left = RdEnRect.right + 5;
		WrEnRect.bottom = WrEnRect.top + 25;
		WrEnRect.right = WrEnRect.left + 60;
		Controls[i].WrEnBtn.ShowWindow(SW_SHOWNORMAL);

		// Adjust position of labels for control columns
		if(i == nFirstControlNum) {
			pStatic = (CStatic *)(GetDlgItem(IDC_STATIC_MEMTYPE));
			TmpRect = TypeRect;
			TmpRect.top -= 30;
			TmpRect.bottom -= 30;
			pStatic->MoveWindow(TmpRect);

			pStatic = (CStatic *)(GetDlgItem(IDC_STATIC_ADDR));
			TmpRect = AddrRect;
			TmpRect.top -= 30;
			TmpRect.bottom -= 30;
			pStatic->MoveWindow(TmpRect);

			pStatic = (CStatic *)(GetDlgItem(IDC_STATIC_DATAVAL));
			TmpRect = ValueRect;
			TmpRect.top -= 30;
			TmpRect.bottom -= 30;
			pStatic->MoveWindow(TmpRect);

			pStatic = (CStatic *)(GetDlgItem(IDC_STATIC_LOOPEN));
			int nMid = WrEnRect.left - RdEnRect.right;
			TmpRect.left = RdEnRect.right + nMid - 30;
			TmpRect.right = TmpRect.left + 65;
			TmpRect.top = RdEnRect.top - 30;
			TmpRect.bottom = TmpRect.top + 25;
			pStatic->MoveWindow(TmpRect);
		}

		// Adjust number labels for controls
		switch(i-nFirstControlNum) {
			case 0: pStatic = (CStatic *)(GetDlgItem(IDC_STATIC_P1)); break;
			case 1: pStatic = (CStatic *)(GetDlgItem(IDC_STATIC_P2)); break;
			case 2: pStatic = (CStatic *)(GetDlgItem(IDC_STATIC_P3)); break;
			case 3: pStatic = (CStatic *)(GetDlgItem(IDC_STATIC_P4)); break;
			case 4: pStatic = (CStatic *)(GetDlgItem(IDC_STATIC_P5)); break;
			case 5: pStatic = (CStatic *)(GetDlgItem(IDC_STATIC_P6)); break;
		}

		pStatic->GetWindowRect(TmpRect);
		nHeight = TmpRect.Height();
		nWidth = TmpRect.Width();
		TmpRect.top = TypeRect.top;
		TmpRect.bottom = TmpRect.top + nHeight;
		TmpRect.right = TypeRect.left - 5;
		TmpRect.left = TmpRect.right - nWidth;
		str.Format("%d",i+1);
		pStatic->SetWindowText(str);
		pStatic->MoveWindow(TmpRect);

		// Move to next row
		TypeRect.top += 40;
		TypeRect.bottom += 40;
	}
}

void CMemAccessDlg::LoadMemoryAreaData(UINT nID_COMBO_BOX)
{
	CComboBox * pCombo;

	pCombo = (CComboBox *)GetDlgItem(nID_COMBO_BOX);

	pCombo->AddString("MEM Map IO");
	pCombo->AddString("SFR");
	pCombo->AddString("CPU RAM");
	pCombo->AddString("CPU ROM");
	pCombo->SetCurSel(0);
}

void CMemAccessDlg::LoadPIUAddressData(CString filename, UINT nID_COMBO_BOX)
{
	CComboBox * pCombo;

	pCombo = (CComboBox *)GetDlgItem(nID_COMBO_BOX);
        
	char buf[120];

	std::ifstream addrfile;
	addrfile.open(filename);

	if( addrfile.is_open() ) {
		while( !addrfile.eof() ) {
			addrfile >> buf;
			pCombo->AddString(buf);
		}
		addrfile.close();
	}
	pCombo->SetCurSel(0);
}

void CMemAccessDlg::MakeControlLoopList(void)
{
	CButton *pBtn;

	nNextGroupNumberToLoop = -1;

	// Find addresses to read
	for(int i=0; i<NUM_CONTROL_GROUPS; i++) {
		pBtn = (CButton *)GetDlgItem(2005 + (i*7));
		if(pBtn->GetCheck()) {
			ReadControlsToLoop[i] = true;
			if(nNextGroupNumberToLoop == -1) {
				nNextGroupNumberToLoop = i;
			}
		} else
			ReadControlsToLoop[i] = false;
	}

	// Find addresses to write
	for(i=0; i<NUM_CONTROL_GROUPS; i++) {
		pBtn = (CButton *)GetDlgItem(2006 + (i*7));
		if(pBtn->GetCheck()) {
			WriteControlsToLoop[i] = true;
			if(nNextGroupNumberToLoop == -1) {
				nNextGroupNumberToLoop = i;
			}
		} else
			WriteControlsToLoop[i] = false;
	}
}

void CMemAccessDlg::Read(int nID_COMBO_BOX, int memory_area_id, CString &resultstring)
{
	WORD AddressContents;
	bErrorDetected = false;

    // grab the current address from the dialog
    UpdateData(TRUE);
    CString szAddress;

	GetDlgItemText(nID_COMBO_BOX,szAddress);
			
    unsigned int Address2Read = strtol(szAddress, NULL, 16);

	CComboBox *pcb = (CComboBox *)GetDlgItem(memory_area_id);
	
	if(bAdvancedBootloaderAvailable) {
		MemoryArea ma = DisableAll;

		switch (pcb->GetCurSel()) {
		default:
			ma = DisableAll;
			break;
		case MAI_SFR:
			ma = CPU_SFR;
			break;
		case MAI_CPU_RAM:
			ma = CPU_RAM;
			break;
		case MAI_CPU_ROM:
			ma = CPU_Code;
			break;
		}

		if (!commandEngine.SelectMemory(ma)) {
        	commandEngine.GetErrorMsgString(m_Status);
        	bErrorDetected = true;
		}
	}
	
	if(!bErrorDetected) {
		// read data from address
    	if(commandEngine.Peek( Address2Read, &AddressContents )) {

        	resultstring.Format("0x%X", AddressContents);

        	m_Status = "";
        	m_Status = "Address " + szAddress + "=" + resultstring;
    	} else {
        	commandEngine.GetErrorMsgString(m_Status);
        	bErrorDetected = true;
    	}

    	if(bDataLoggingEnabled) {
        	logfile << (LPCTSTR)m_Status << std::endl;
		}
    }

    // update the dialog
    UpdateData(FALSE);	
}

void CMemAccessDlg::Write(int nID_COMBO_BOX, int memory_area_id, CString &datastring)
{
	// grab the current address and data from the dialog
    UpdateData(TRUE);
    CString szAddress;
    GetDlgItemText(nID_COMBO_BOX, szAddress);
	CComboBox *pcb = (CComboBox *)GetDlgItem(memory_area_id);
    unsigned int Address2Write = strtol(szAddress, NULL, 16);
    WORD Data2Write = static_cast<WORD>(strtol(datastring, NULL, 16));

    // write data to address
    bErrorDetected = false;

	if(bAdvancedBootloaderAvailable) {
		MemoryArea ma = DisableAll;

		switch (pcb->GetCurSel()) {
		default:
			ma = DisableAll;
			break;
		case MAI_SFR:
			ma = CPU_SFR;
			break;
		case MAI_CPU_RAM:
			ma = CPU_RAM;
			break;
		case MAI_CPU_ROM:
			ma = CPU_Code;
			break;
		}

		if (!commandEngine.SelectMemory(ma)) {
        	commandEngine.GetErrorMsgString(m_Status);
        	bErrorDetected = true;
		}
	}

	if(!bErrorDetected) {
    	if(commandEngine.Poke( Address2Write, Data2Write, bEnableSafetyCriticalWrites )) {

        	CString OldValue;
        	OldValue.Format("0x%X", Data2Write); 

        	m_Status = "";
        	m_Status = OldValue + " written to address " + szAddress;
    	} else {
        	commandEngine.GetErrorMsgString(m_Status);
        	bErrorDetected = true;
    	}

    	if(bDataLoggingEnabled) {
        	logfile << (LPCTSTR)m_Status << std::endl;
    	}
	}

    // update the dialog, probably not needed
    UpdateData(FALSE);
}

